#define TAG "SIM7000"

#include "SIM7000.h"

#include <driver/gpio.h>
#include <driver/rtc_io.h>
#include <driver/uart.h>
#include <esp_err.h>
#include <esp_log.h>
#include <freertos/semphr.h>
#include <mbedtls/base64.h>
#include <stdint.h>
#include <string.h>

#include "RSA.h"
#include "SIM7000.h"
#include "cert.h"
#include "storage.h"
#include "utils.h"

#define DEFAULT_TIMEOUT 100

char sim7000_buffer[CONFIG_SIM7000_BUFFER_SIZE + 1];

static bool full_init = false;

SemaphoreHandle_t sim7000_mutex;

int SIM7000_generic_send(char* input, uint32_t timeout_ms) {
    uart_write_bytes(CONFIG_SIM7000_UART_DEVICE, input, strlen(input));
    uart_write_bytes(CONFIG_SIM7000_UART_DEVICE, "\r", 1);

    uint32_t end_time = GET_TIME() + timeout_ms;
    int recv = 0;
    while (1) {
        int temp_recv = uart_read_bytes(CONFIG_SIM7000_UART_DEVICE, ((uint8_t*)sim7000_buffer) + recv, CONFIG_SIM7000_BUFFER_SIZE - recv, 10 / portTICK_RATE_MS);

        if (temp_recv > 0) {
            sim7000_buffer[recv + temp_recv] = '\0';
            ESP_LOGV(TAG, "sent \"%s\"; received \"%s\"", input, sim7000_buffer + recv);

            recv += temp_recv;
            end_time = GET_TIME() + timeout_ms;

            if (strchr(sim7000_buffer, '\r') != NULL || recv >= CONFIG_SIM7000_BUFFER_SIZE) {
                break;
            }
        }

        if (GET_TIME() > end_time) {
            break;
        }

        delay(10);
    }

    return recv;
}

int SIM7000_send(char* input) {
    return SIM7000_generic_send(input, DEFAULT_TIMEOUT);
}

esp_err_t SIM7000_send_expect(char* input, uint32_t timeout_ms, char* expected_output) {
    int recv = SIM7000_generic_send(input, timeout_ms);

    if (recv == 0) {
        ESP_LOGW(TAG, "Timeout %s", input);
        return -ESP_ERR_TIMEOUT;
    } else if (recv - 4 == strlen(expected_output) && strncmp(expected_output, sim7000_buffer + 2, strlen(expected_output)) == 0) {
        return ESP_OK;
    } else {
        ESP_LOGE(TAG, "Expect failed for command \"%s\": \"%s\"(received)", input, sim7000_buffer + 2);
        return ESP_FAIL;
    }
}

esp_err_t SIM7000_expect_ok(char* input) {
    return SIM7000_send_expect(input, DEFAULT_TIMEOUT, "OK");
}
esp_err_t SIM7000_expect_nonstrict(char* input, char* output) {
    SIM7000_send(input);
    char* ptr = strstr(sim7000_buffer, output);

    return ptr == NULL ? ESP_FAIL : ESP_OK;
}

esp_err_t SIM7000_config_operator() {
    ESP_LOGW(TAG, "Configure network operator");
    char cops[128] = "AT+COPS=1,0,";

    SIM7000_generic_send("AT+COPS=?", 120000);
    char* ptr = strstr(sim7000_buffer, "+COPS: ");

    if (ptr == NULL) {
        ESP_LOGE(TAG, "AT+COPS=? failed %s", sim7000_buffer);
        return ESP_FAIL;
    }

    char* firstComma = strchr(sim7000_buffer, ',') + 1;
    if (firstComma == NULL) {
        ESP_LOGE(TAG, "AT+COPS=? failed %s %p", sim7000_buffer, firstComma);
        return ESP_FAIL;
    }
    char* secondComma = strchr(firstComma, ',');

    if (secondComma == NULL) {
        ESP_LOGE(TAG, "AT+COPS=? failed %s %p %p", sim7000_buffer, firstComma, secondComma);
        return ESP_FAIL;
    }
    int len = strlen(cops);
    strncpy(cops + len, firstComma, secondComma - firstComma);
    cops[len + secondComma - firstComma] = '\0';

    return SIM7000_generic_send(cops, 120000);
}

void SIM7000_upload_file(char* file_name, char* content) {
    char temp[128];
    sprintf(temp, "AT+CFSWFILE=3,\"%s\",0,%d,5000", file_name, strlen(content));

    SIM7000_wait_flush();
    SIM7000_send_expect(temp, 5000, "DOWNLOAD");
    SIM7000_generic_send(content, 5000);
}

esp_err_t SIM7000_load_certs() {
    esp_err_t err = ESP_OK;
    // load SSL files
    err |= SIM7000_send_expect("AT+CFSINIT", 5000, "OK");
    SIM7000_upload_file("server.pem", SERVER_CERT);
    SIM7000_upload_file("client.pem", CLIENT_CERT);
    SIM7000_upload_file("client.key", CLIENT_KEY);
    err |= SIM7000_send_expect("AT+CFSTERM", 5000, "OK");

    err |= SIM7000_expect_ok("AT+CSSLCFG=\"convert\",2,\"server.pem\"");
    err |= SIM7000_expect_ok("AT+CSSLCFG=\"convert\",1,\"client.pem\",\"client.key\"");

    return err == ESP_OK ? ESP_OK : ESP_FAIL;
}

void SIM7000_config() {
    SIM7000_send_expect("AT+CFUN=1", 10000, "OK");  // enable full functionality
    SIM7000_expect_ok("AT+CBATCHK=0");              // disable battery check
    SIM7000_expect_ok("AT+CNMP=13");                // GPRS only
    SIM7000_expect_ok("AT+CSCLK=1");                // enable sleep mode via DTR
    SIM7000_expect_ok("AT+CGNSMOD=1,1,0,0");        // set GPS + GLONASS
    SIM7000_send("AT+CMGF=1");                      // select SMS text mode
    SIM7000_send("AT+CSCS=\"GSM\"");                // set SMS charset

    // configure global SSL
    SIM7000_expect_ok("AT+CSSLCFG=\"sslversion\",0,3");  // TLS1.2
    SIM7000_expect_ok("AT+CSSLCFG=\"protocol\",0,1");
    SIM7000_expect_ok("AT+CSSLCFG=\"sni\",0,\"adse.ro:444\"");
    SIM7000_expect_ok("AT+CSSLCFG=\"ignorertctime\",0,1");
    SIM7000_expect_ok("AT+CSSLCFG=\"ciphersuite\",0,0,0xC02C");  // ECDH ECDSA AES256-GCM SHA384
    SIM7000_expect_ok("AT+CSSLCFG=\"ciphersuite\",0,1,0xC030");  // ECDH RSA   AES256-GCM SHA384
    SIM7000_expect_ok("AT+CSSLCFG=\"ciphersuite\",0,2,0xC02B");  // ECDH ECDSA AES128-GCM SHA256
    SIM7000_expect_ok("AT+CSSLCFG=\"ciphersuite\",0,3,0xC02F");  // ECDH RSA   AES128-GCM SHA256

    if (!storage.loaded_sim7000_certs) {
        if (SIM7000_load_certs() == ESP_OK) {
            storage.loaded_sim7000_certs = true;
            storage_dirty = true;
            ESP_LOGI(TAG, "Loaded certs");
        } else {
            ESP_LOGE(TAG, "Failed to write certificates to SIM7000");
        }
    }
}

esp_err_t SIM7000_connect_network() {
    SIM7000_send("AT+COPS?");

    if (strncmp(sim7000_buffer + 2, "+COPS: 1", 8) != 0) {
        if (SIM7000_config_operator() != ESP_OK) {
            ESP_LOGE(TAG, "unrecoverable SIM7000 error");
            return ESP_FAIL;
        }
    }
    SIM7000_send("AT+COPS?");

    int retries = 0;
    while (retries < 3) {
        if (SIM7000_send_expect("AT+CGATT=1", 30000, "OK") == ESP_OK) {
            break;
        }
        retries++;
    }
    return ESP_OK;
}

xQueueHandle sim7000_int_queue = NULL;

static void IRAM_ATTR sim7000_int_isr_handler(void* arg) {
    atomic_fetch_add(&can_sleep, 1);
    uint8_t data = 1;
    xQueueSendFromISR(sim7000_int_queue, &data, NULL);
}

static void sim7000_int_task(void* arg) {
    uint8_t data;
    for (;;) {
        if (xQueueReceive(sim7000_int_queue, &data, portMAX_DELAY)) {
            ESP_LOGI(TAG, "SIM7000 GPIO intr %d\n", gpio_get_level(CONFIG_SIM7000_RI_GPIO));

            int retries = 0;
            while (retries < 3) {
                retries++;
                if (xSemaphoreTake(sim7000_mutex, 500) != pdTRUE) {
                    continue;
                }

                uint8_t sms_available = SIM7000_num_sms_available();
                xSemaphoreGive(sim7000_mutex);

                if (sms_available < SMS_PIECES_FOR_VALID_PACKET) {
                    delay(500);
                } else {
                    SIM7000_check_SMS(decrypted_message_from_sms);
                    break;
                }
            }

            if (data == 1) {
                atomic_fetch_add(&can_sleep, -1);
            }
        }
    }
}

void SIM7000_wait_flush() {
    while (SIM7000_expect_nonstrict("AT", "OK") != ESP_OK) {
        delay(100);
    }
}

void SIM7000_sleep() {
    while (xSemaphoreTake(sim7000_mutex, portMAX_DELAY) != pdTRUE) {
        delay(500);
    }

    // close TSL connection (leaving it open draws 18mA in sleep)
    SIM7000_send("AT+CACLOSE=0");
    SIM7000_deactivate_location();

    SIM7000_expect_nonstrict("AT+CSCLK=1", "OK");
    delay(100);

    gpio_set_level(CONFIG_SIM7000_DTR_GPIO, 1);
    rtc_gpio_hold_en(CONFIG_SIM7000_DTR_GPIO);
}

void SIM7000_after_sleep_init() {
    if (!full_init) {
        atomic_fetch_add(&init_counter, 1);
        ESP_LOGI(TAG, "after sleep init");
    }

    uart_config_t uart_config = {
        .baud_rate = CONFIG_SIM7000_BAUDRATE,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};

    uart_param_config(CONFIG_SIM7000_UART_DEVICE, &uart_config);
    uart_set_pin(CONFIG_SIM7000_UART_DEVICE, CONFIG_SIM7000_TX_GPIO, CONFIG_SIM7000_RX_GPIO, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

    uart_driver_install(CONFIG_SIM7000_UART_DEVICE, CONFIG_SIM7000_BUFFER_SIZE, CONFIG_SIM7000_BUFFER_SIZE, 0, NULL, 0);

    sim7000_mutex = xSemaphoreCreateMutex();
    if (sim7000_mutex == NULL) {
        ESP_LOGE(TAG, "Can't create sim7000_mutex");
    }
    xSemaphoreTake(sim7000_mutex, portMAX_DELAY);

    rtc_gpio_hold_dis(CONFIG_SIM7000_DTR_GPIO);
    rtc_gpio_deinit(CONFIG_SIM7000_DTR_GPIO);
    gpio_reset_pin(CONFIG_SIM7000_DTR_GPIO);
    gpio_set_direction(CONFIG_SIM7000_DTR_GPIO, GPIO_MODE_OUTPUT);
    gpio_set_level(CONFIG_SIM7000_DTR_GPIO, 0);

    rtc_gpio_deinit(CONFIG_SIM7000_RI_GPIO);
    gpio_reset_pin(CONFIG_SIM7000_RI_GPIO);
    gpio_set_direction(CONFIG_SIM7000_RI_GPIO, GPIO_MODE_INPUT);

    sim7000_int_queue = xQueueCreate(10, sizeof(uint8_t));
    xTaskCreatePinnedToCore(sim7000_int_task, "sim7000_int_task", 2048 * 4, NULL, configMAX_PRIORITIES, NULL, APP_CPU);

    gpio_set_intr_type(CONFIG_SIM7000_RI_GPIO, GPIO_INTR_NEGEDGE);
    gpio_intr_enable(CONFIG_SIM7000_RI_GPIO);
    gpio_isr_handler_add(CONFIG_SIM7000_RI_GPIO, sim7000_int_isr_handler, NULL);

    do {
        if (SIM7000_expect_ok("ATE0") == ESP_OK) {
            break;
        }

        delay(500);
    } while (1);
    ESP_LOGI(TAG, "SIM7000 turned on");

    SIM7000_send("AT+CCLK=\"20/08/23,06:00:00+08\"");  // make sure that certs are valid

    if (!full_init) {
        SIM7000_wait_flush();
        xSemaphoreGive(sim7000_mutex);

        atomic_fetch_add(&init_counter, -1);
        ESP_LOGI(TAG, "after sleep init finished");
        vTaskDelete(NULL);
    }
}

void SIM7000_full_init() {
    full_init = true;
    atomic_fetch_add(&init_counter, 1);
    ESP_LOGI(TAG, "full init");

    SIM7000_after_sleep_init();
    SIM7000_config();

    SIM7000_wait_flush();
    xSemaphoreGive(sim7000_mutex);

    atomic_fetch_add(&init_counter, -1);
    ESP_LOGI(TAG, "full init finished");
    vTaskDelete(NULL);
}
