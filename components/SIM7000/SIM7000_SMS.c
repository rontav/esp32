#define TAG "SIM7000"

#include <driver/gpio.h>
#include <driver/rtc_io.h>
#include <driver/uart.h>
#include <esp_err.h>
#include <esp_log.h>
#include <freertos/semphr.h>
#include <mbedtls/base64.h>
#include <stdint.h>
#include <string.h>

#include "RSA.h"
#include "SIM7000.h"
#include "cert.h"
#include "adsero.h"
#include "storage.h"

#define SMS_MAX_CHARS 160
#define MULTIPART_SMS_HEADER 3
#define MULTIPART_SMS_MAX_CHARS (153 - MULTIPART_SMS_HEADER)

char decrypted_message_from_sms[ENC_LEN_BYTE];

char sms_buffer[SMS_MAX_CHARS * (SMS_PIECES_FOR_VALID_PACKET - 1) + 1];
char encrypted_message_partial[MULTIPART_SMS_MAX_CHARS * SMS_PIECES_FOR_VALID_PACKET + 1];

uint8_t is_custom_multipart_sms(char* buffer) {
    return buffer[0] == buffer[2] && (buffer[0] == '!') && buffer[1] >= '0' && buffer[1] <= '9';
}

int SIM7000_num_sms_available() {
    char* ptr;
    char* pEnd;

    SIM7000_send("AT+CPMS?");
    ptr = strstr(sim7000_buffer, "\"SM\",");
    if (ptr == NULL) {
        return 0;
    }
    ptr += strlen("\"SM\",");

    uint8_t sms_available = strtol(ptr, &pEnd, 10);
    ESP_LOGV(TAG, "%d SMS available", sms_available);
    return sms_available;
}

esp_err_t SIM7000_delete_sms(uint8_t index) {
    char temp[16];
    sprintf(temp, "AT+CMGD=%d", index);
    return SIM7000_expect_ok(temp);
}

esp_err_t SIM7000_delete_all_sms() {
    return SIM7000_expect_ok("AT+CMGD=0,1");
}

uint16_t SIM7000_read_sms(uint8_t index, uint8_t mark_as_read, char* sms_content) {
    uint16_t size = 0;

    char temp[16];
    sprintf(temp, "AT+CMGR=%d,%d", index, mark_as_read == 0 ? 1 : 0);
    SIM7000_send(temp);

    char* ptr = strstr(sim7000_buffer, "\"\r\n");
    if (ptr != NULL) {
        ptr += strlen("\"\r\n");
        strncpy(sms_content + size, ptr, strlen(ptr));
        size += strlen(ptr);

        while (1) {
            int recv = SIM7000_generic_send("AT", 1000);
            if (recv <= 0) {
                continue;
            }
            if (recv - 4 == strlen("OK") && strncmp("OK", sim7000_buffer + 2, strlen("OK")) == 0) {
                break;
            }

            ptr = strstr(sim7000_buffer, "\r\n");
            if (ptr != NULL) {
                strncpy(sms_content + size, sim7000_buffer, ptr - sim7000_buffer);
                size += ptr - sim7000_buffer;
            } else {
                strcpy(sms_content + size, sim7000_buffer);
                size += strlen(sim7000_buffer);
            }
            delay(50);
        }
        sms_content[size] = '\0';
    }

    return size;
}

int SIM7000_check_SMS(char* decrypted_message) {
    ESP_LOGI(TAG, "Checking SMS for valid encrypted messages");

    if (xSemaphoreTake(sim7000_mutex, 10) != pdTRUE) {
        ESP_LOGI(TAG, "Failed to aquire mutex");
        return ESP_FAIL;
    }

    uint8_t sms_available = SIM7000_num_sms_available();
    if (sms_available < SMS_PIECES_FOR_VALID_PACKET) {
        goto fail;
    }

    uint8_t pieces_written = 0;
    for (uint8_t i = 0; i < sms_available; i++) {
        if (SIM7000_read_sms(i, true, sms_buffer) > 0) {
            decode_hex_in_place(sms_buffer);
            ESP_LOGV(TAG, "SMS %d: %s", i, sms_buffer);

            if (is_custom_multipart_sms(sms_buffer)) {
                uint8_t piece = sms_buffer[1] - '0';

                char* ptr = strchr(sms_buffer + MULTIPART_SMS_HEADER, '!');
                if (ptr == NULL && piece == (SMS_PIECES_FOR_VALID_PACKET - 1)) {
                    ESP_LOGE(TAG, "invalid piece %d, sms index %d", piece, i);
                    continue;
                }

                int sms_buffer_size;
                if (piece == SMS_PIECES_FOR_VALID_PACKET - 1) {
                    sms_buffer_size = ptr - sms_buffer - MULTIPART_SMS_HEADER;
                    encrypted_message_partial[piece * MULTIPART_SMS_MAX_CHARS + sms_buffer_size] = '\0';
                } else {
                    sms_buffer_size = strlen(sms_buffer) - MULTIPART_SMS_HEADER;
                }

                strncpy(encrypted_message_partial + piece * MULTIPART_SMS_MAX_CHARS, sms_buffer + MULTIPART_SMS_HEADER, sms_buffer_size);

                pieces_written++;
            } else {
                ESP_LOGE(TAG, "Invalid SMS %d, delete! (%s)", i, sms_buffer);
                SIM7000_delete_sms(i);
            }
        } else {
            ESP_LOGE(TAG, "Failed to read SMS %d", i);
        }
    }
    encrypted_message_partial[MULTIPART_SMS_MAX_CHARS * SMS_PIECES_FOR_VALID_PACKET] = '\0';

    if (pieces_written == SMS_PIECES_FOR_VALID_PACKET) {
        unsigned char encrypted_message[ENC_LEN_BYTE];
        size_t bytesWritten = 0;

        int err = mbedtls_base64_decode(
            encrypted_message,
            ENC_LEN_BYTE,
            &bytesWritten,
            (unsigned char*)encrypted_message_partial,
            strlen(encrypted_message_partial));

        if (err != 0) {
            ESP_LOGE(TAG, "Base64 decode error: %d", err);
        } else {
            err = rsa_decrypt(encrypted_message, (uint8_t*)decrypted_message);

            if (err > 0) {
                ESP_LOGE(TAG, "RSA decrypt error: %d", err);
            } else {
                if (strstr(decrypted_message, "STOLEN") != NULL) {
                    ESP_LOGW(TAG, "Scooter locked with SMS");

                    if (adsero_set_status(STOLEN)) {
                        SIM7000_delete_all_sms();
                    }
                } else {
                    ESP_LOGW(TAG, "Received unknown SMS: \"%s\". DELETED!", decrypted_message);

                    SIM7000_delete_all_sms();
                }

                xSemaphoreGive(sim7000_mutex);
                return ESP_OK;
            }
        }
    }

fail:
    ESP_LOGI(TAG, "No valid encrypted SMS found");
    SIM7000_wait_flush();
    xSemaphoreGive(sim7000_mutex);
    return ESP_FAIL;
}
