#define TAG "SIM7000"

#include <esp_err.h>
#include <esp_log.h>
#include <string.h>

#include "SIM7000.h"

void SIM7000_activate_location() {
    SIM7000_expect_ok("AT+CGNSPWR=1");
}
void SIM7000_deactivate_location() {
    SIM7000_expect_ok("AT+CGNSPWR=0");
}

esp_err_t SIM7000_get_location(Location* location, uint32_t timeout_ms) {
    while (xSemaphoreTake(sim7000_mutex, portMAX_DELAY) != pdTRUE) {
        delay(500);
    }

    memset(location, 0, sizeof(Location));

    uint32_t end_time = GET_TIME() + timeout_ms;
    do {
        if (GET_TIME() > end_time) {
            break;
        }

        SIM7000_send("AT+CGNSINF");

        char* ptr = strstr(sim7000_buffer, "+CGNSINF: ");
        if (ptr) {
            ptr += strlen("+CGNSINF: ");  // skip over run status

            uint8_t location_active = strtol(ptr, &ptr, 10);
            ptr++;

            if (!location_active) {
                xSemaphoreGive(sim7000_mutex);
                return ESP_FAIL;
            }

            location->fix = strtol(ptr, &ptr, 10);
            ptr++;

            if (location->fix == 0) {
                delay(500);
                continue;
            }

            location->scooter_timestamp = strtod(ptr, &ptr);
            ptr++;

            location->latitude = strtod(ptr, &ptr);
            ptr++;
            location->longitude = strtod(ptr, &ptr);
            ptr++;

            location->altitude = strtof(ptr, &ptr);
            ptr++;

            location->speed = strtof(ptr, &ptr);
            ptr++;
            location->heading = strtof(ptr, &ptr);
            ptr++;
            ptr = strchr(ptr, ',') + 1;  // skip fix mode
            ptr = strchr(ptr, ',') + 1;  // skip reserved

            location->hdop = strtof(ptr, &ptr);
            ptr++;
            location->pdop = strtof(ptr, &ptr);
            ptr++;
            location->vdop = strtof(ptr, &ptr);
            ptr++;
            ptr = strchr(ptr, ',') + 1;  // skip reserved

            uint8_t gps_in_view = strtol(ptr, &ptr, 10);
            ptr++;
            location->satellites_used = strtol(ptr, &ptr, 10);
            ptr++;
            uint8_t glonass_in_view = strtol(ptr, &ptr, 10);
            location->satellites_in_view = gps_in_view + glonass_in_view;
            ptr++;
            ptr = strchr(ptr, ',') + 1;  // skip reserved
            ptr = strchr(ptr, ',') + 1;  // skip undocumented

            location->cn0_max = strtol(ptr, NULL, 10);

            SIM7000_wait_flush();
            xSemaphoreGive(sim7000_mutex);

            return ESP_OK;
        }

        delay(100);
    } while (1);

    SIM7000_wait_flush();
    xSemaphoreGive(sim7000_mutex);

    return ESP_ERR_TIMEOUT;
}

void SIM7000_print_location(Location* location) {
    ESP_LOGI(TAG, "Fix: %d", location->fix);

    ESP_LOGI(TAG, "GPS Timestamp: %f", location->scooter_timestamp);

    ESP_LOGI(TAG, "Latitude: %f", location->latitude);
    ESP_LOGI(TAG, "Longitude: %f", location->longitude);

    ESP_LOGI(TAG, "Altitude: %f", location->altitude);
    ESP_LOGI(TAG, "Speed: %f", location->speed);
    ESP_LOGI(TAG, "Heading: %f", location->heading);

    ESP_LOGI(TAG, "HDOP: %f", location->hdop);
    ESP_LOGI(TAG, "PDOP: %f", location->pdop);
    ESP_LOGI(TAG, "VDOP: %f", location->vdop);

    ESP_LOGI(TAG, "Satellites in view: %d", location->satellites_in_view);
    ESP_LOGI(TAG, "Satellites used: %d", location->satellites_used);
    ESP_LOGI(TAG, "CN0 max: %d", location->cn0_max);
}