#ifndef _SIM7000_H_
#define _SIM7000_H_

#include <esp_err.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <sdkconfig.h>

#include "cert.h"
#include "utils.h"

#define SMS_PIECES_FOR_VALID_PACKET 3

extern char sim7000_buffer[CONFIG_SIM7000_BUFFER_SIZE + 1];
extern char decrypted_message_from_sms[ENC_LEN_BYTE];

extern SemaphoreHandle_t sim7000_mutex;
extern xQueueHandle sim7000_int_queue;

#pragma pack(push, 1)
typedef struct {
    double scooter_timestamp;
    double latitude, longitude;
    float altitude;
    float speed;
    float heading;
    float hdop, pdop, vdop;
    uint8_t satellites_in_view;
    uint8_t satellites_used;
    uint8_t cn0_max;
    uint8_t fix;
} Location;
#pragma pack(pop)

void SIM7000_after_sleep_init();
void SIM7000_full_init();
void SIM7000_sleep();
void SIM7000_wait_flush();

int SIM7000_generic_send(char*, uint32_t);
int SIM7000_send(char*);
esp_err_t SIM7000_send_expect(char*, uint32_t, char*);
esp_err_t SIM7000_expect_ok(char*);
esp_err_t SIM7000_expect_nonstrict(char*, char*);

esp_err_t SIM7000_delete_sms(uint8_t);
esp_err_t SIM7000_delete_all_sms();
int SIM7000_num_sms_available();
uint16_t SIM7000_read_sms(uint8_t, uint8_t, char*);
int SIM7000_check_SMS(char*);

esp_err_t SIM7000_connect_network();
esp_err_t SIM7000_load_certs();

void SIM7000_activate_location();
void SIM7000_deactivate_location();
esp_err_t SIM7000_get_location(Location* location, uint32_t timeout_ms);
void SIM7000_print_location(Location* location);

#endif