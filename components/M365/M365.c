#define TAG "M365"

#include "M365.h"

#include <driver/gpio.h>
#include <driver/rtc_io.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

#include "proto.h"
#include "storage.h"

static bool full_init = false;

m365_channel m365;
m365_channel dash;

void M365_send(m365_channel *channel) {
    uart_write_bytes(channel->uart_device, (const char *)(channel->tx), channel->tx_size);
    channel->rx_offset += channel->tx_size;
    channel->tx_size = 0;
}

void M365_recv(m365_channel *channel) {
    uint32_t recv = uart_read_bytes(channel->uart_device, channel->rx, 32, 10 / portTICK_RATE_MS);

    channel->rx_size = recv;
};

inline void M365_copy_tx_chan(m365_channel *channel, uint8_t *data, uint32_t size) {
    if (size > CONFIG_M365_BUFFER_SIZE) {
        ESP_LOGE(TAG, "%s: Communication channel buffer to small %d > %d\n", __func__, size, CONFIG_M365_BUFFER_SIZE);
        return;
    }

    channel->tx_size = size;

    memcpy(channel->tx, data, size);
}

static void tx_task(void *arg) {
    m365_channel *channel = (m365_channel *)arg;

    while (1) {
        xSemaphoreTake(channel->tx_sem, portMAX_DELAY);
        if (channel->tx_size > 0) {
            M365_send(channel);
        }

        xSemaphoreGive(channel->rx_sem);
    }
}

static void rx_task(void *arg) {
    m365_channel *channel = (m365_channel *)arg;
    m365_channel *other_channel;
    if (channel == &m365) {
        other_channel = &dash;
    } else {
        other_channel = &m365;
    }

    while (1) {
        /* Wait to receive signal from tx_task */
        xSemaphoreTake(channel->rx_sem, portMAX_DELAY);

        /* Receive data from channel */
        M365_recv(channel);
        proto_command(channel);
        channel->rx_offset = 0;

        xSemaphoreGive(other_channel->tx_sem);
    }
}

void M365_after_sleep_init(void) {
    if (!full_init) {
        atomic_fetch_add(&init_counter, 1);
        ESP_LOGI(TAG, "after sleep init");
    }

    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    };

    uart_param_config(CONFIG_M365_UART_DEVICE, &uart_config);
    uart_set_pin(CONFIG_M365_UART_DEVICE, CONFIG_M365_TX_GPIO, CONFIG_M365_RX_GPIO, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

    uart_driver_install(CONFIG_M365_UART_DEVICE, CONFIG_M365_BUFFER_SIZE * 2, 0, 0, NULL, 0);
    m365.uart_device = CONFIG_M365_UART_DEVICE;
    m365.rx_offset = 0;
    m365.rx_sem = xSemaphoreCreateBinary();
    m365.tx_sem = xSemaphoreCreateBinary();
    xSemaphoreGive(m365.tx_sem);

    uart_param_config(CONFIG_DASH_UART_DEVICE, &uart_config);
    uart_set_pin(CONFIG_DASH_UART_DEVICE, CONFIG_DASH_TX_GPIO, CONFIG_DASH_RX_GPIO, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

    uart_driver_install(CONFIG_DASH_UART_DEVICE, CONFIG_M365_BUFFER_SIZE * 2, 0, 0, NULL, 0);
    dash.uart_device = CONFIG_DASH_UART_DEVICE;
    dash.rx_offset = 0;
    dash.rx_sem = xSemaphoreCreateBinary();
    dash.tx_sem = xSemaphoreCreateBinary();

    xTaskCreatePinnedToCore(rx_task, "rx_task_m365", 1024 * 4, &m365, configMAX_PRIORITIES - 1, NULL, M365_CPU);
    xTaskCreatePinnedToCore(rx_task, "rx_task_dash", 1024 * 4, &dash, configMAX_PRIORITIES - 1, NULL, M365_CPU);

    xTaskCreatePinnedToCore(tx_task, "tx_task_m365", 1024 * 4, &m365, configMAX_PRIORITIES - 1, NULL, M365_CPU);
    xTaskCreatePinnedToCore(tx_task, "tx_task_dash", 1024 * 4, &dash, configMAX_PRIORITIES - 1, NULL, M365_CPU);

    rtc_gpio_hold_dis(CONFIG_DASH_POWER_CTRL_GPIO);
    rtc_gpio_deinit(CONFIG_DASH_POWER_CTRL_GPIO);
    gpio_reset_pin(CONFIG_DASH_POWER_CTRL_GPIO);
    gpio_set_direction(CONFIG_DASH_POWER_CTRL_GPIO, GPIO_MODE_OUTPUT);
    if (storage.adsero_status == ACTIVE) {
        gpio_set_level(CONFIG_DASH_POWER_CTRL_GPIO, 1);
    }
    rtc_gpio_hold_en(CONFIG_DASH_POWER_CTRL_GPIO);

    gpio_reset_pin(CONFIG_M365_42V_CTRL_GPIO);
    gpio_set_direction(CONFIG_M365_42V_CTRL_GPIO, GPIO_MODE_OUTPUT);
    gpio_set_level(CONFIG_M365_42V_CTRL_GPIO, 0);
    // rtc_gpio_hold_en(CONFIG_M365_42V_CTRL_GPIO);

    if (!full_init) {
        atomic_fetch_add(&init_counter, -1);
        ESP_LOGI(TAG, "after sleep init finished");
    }
}

void M365_full_init() {
    full_init = true;
    atomic_fetch_add(&init_counter, 1);
    ESP_LOGI(TAG, "full init");

    M365_after_sleep_init();

    atomic_fetch_add(&init_counter, -1);
    ESP_LOGI(TAG, "full init finished");
}