#ifndef _M365_H_
#define _M365_H_

#include <driver/uart.h>
#include <freertos/semphr.h>
#include <freertos/timers.h>
#include <sdkconfig.h>
#include <stdint.h>

#include "utils.h"

#define CONFIG_DASH_POWER_CTRL_GPIO 25
#define CONFIG_M365_42V_CTRL_GPIO 26

typedef struct {
    uint8_t tx[CONFIG_M365_BUFFER_SIZE];
    uint8_t rx[CONFIG_M365_BUFFER_SIZE];
    int rx_offset;
    int tx_size;
    int rx_size;
    uart_port_t uart_device;
    TimerHandle_t tmr;
    SemaphoreHandle_t tx_sem, rx_sem;
    clock_t last_valid_message_timestamp;
    proto_stat m365_protocol_data;
} m365_channel;

extern m365_channel m365;
extern m365_channel dash;

void M365_after_sleep_init(void);
void M365_full_init(void);
void M365_send(m365_channel *);
void M365_recv(m365_channel *);

void M365_copy_tx_chan(m365_channel *, uint8_t *, uint32_t);

#endif