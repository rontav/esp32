#define TAG "BMX160"

#include "BMX160.h"

#include <driver/gpio.h>
#include <driver/rtc_io.h>
#include <esp_log.h>
#include <esp_sleep.h>
#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

#include "I2C.h"
#include "storage.h"
#include "utils.h"

static bool full_init = false;

float BMX160_get_temperature() {
    int16_t temp;
    BMX160_read(TEMPERATURE, &temp, 2);

    return 23 + temp * 0.001953125f;
}

xQueueHandle accel_int_queue = NULL;

static void IRAM_ATTR accel_int_isr_handler(void* arg) {
    uint8_t data = 1;
    xQueueSendFromISR(accel_int_queue, &data, NULL);
}

static void accel_int_task(void* arg) {
    uint8_t data;
    for (;;) {
        if (xQueueReceive(accel_int_queue, &data, portMAX_DELAY)) {
            if (storage.adsero_status == LOCKED) {
                storage.adsero_status = STOLEN;
                storage.adsero_status_overwrite_server = true;
                storage_save();
            }
        }
    }
}

void BMX160_after_sleep_init() {
    if (!full_init) {
        atomic_fetch_add(&init_counter, 1);
        ESP_LOGI(TAG, "after sleep init");
    }

    rtc_gpio_deinit(CONFIG_ACCEL_INT_GPIO);
    gpio_reset_pin(CONFIG_ACCEL_INT_GPIO);
    gpio_set_direction(CONFIG_ACCEL_INT_GPIO, GPIO_MODE_INPUT);

    accel_int_queue = xQueueCreate(10, sizeof(uint8_t));
    xTaskCreate(accel_int_task, "accel_int_task", 2048, NULL, configMAX_PRIORITIES, NULL);

    gpio_set_intr_type(CONFIG_ACCEL_INT_GPIO, GPIO_INTR_POSEDGE);
    gpio_isr_handler_add(CONFIG_ACCEL_INT_GPIO, accel_int_isr_handler, NULL);
    gpio_intr_enable(CONFIG_ACCEL_INT_GPIO);

    if (!full_init) {
        atomic_fetch_add(&init_counter, -1);
        ESP_LOGI(TAG, "after sleep init finished");
    }
}

void BMX160_full_init() {
    full_init = true;
    atomic_fetch_add(&init_counter, 1);
    ESP_LOGI(TAG, "full init");

    BMX160_after_sleep_init();

    BMX160_write_byte(CMD, 0x11);                                     // set power of accelerometer to normal
    BMX160_write_byte(INT_MOTION + 1, BMX160_ANYMOTION_SENSITIVITY);  // set sensitivity for anymotion
    BMX160_write_byte(ACC_RANGE, BMX160_ACC_RANGE_2G);                // set accelerometer range
    BMX160_write_byte(INT_OUT_CTRL, 0b00001000);                      // activate INT1; set it to push-pull, active low
    BMX160_write_byte(INT_LATCH, 0);                                  // set int pins as non-latched
    BMX160_write_byte(INT_EN, 0b00000111);                            // activate interrupts for anymotion xyz
    BMX160_write_byte(INT_MAP, 0b00000100);                           // map INT1 to anymotion

    atomic_fetch_add(&init_counter, -1);
    ESP_LOGI(TAG, "full init finished");
}
