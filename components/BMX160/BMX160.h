#ifndef _BMX160_H_
#define _BMX160_H_

#include <sdkconfig.h>

#include "utils.h"

#define BMX160_ADDRESS (CONFIG_BMX160_ADDRESS)

#define BMX160_ANYMOTION_SENSITIVITY 10

// Register addresses
typedef enum {
    CHIP_ID = 0x00,  // Should return 0xD8
    ERR_REG = 0x02,
    PMU_STATUS = 0x03,
    DATA = 0x04,
    SENSOERTIME = 0x18,
    STAT = 0x1B,
    INT_STATUS = 0x1C,
    TEMPERATURE = 0x20,
    FIFO_LENGTH = 0x22,
    FIFO_DATA = 0x24,
    ACC_CONF = 0x40,
    ACC_RANGE = 0x41,
    GYR_CONF = 0x42,
    GYR_RANGE = 0x43,
    MAG_CONF = 0x44,
    FIFO_DOWNS = 0x45,
    FIFO_CONFIG = 0x46,
    MAG_IF = 0x4C,
    INT_EN = 0x50,
    INT_OUT_CTRL = 0x53,
    INT_LATCH = 0x54,
    INT_MAP = 0x55,
    INT_DATA = 0x58,
    INT_LOWHIGH = 0x5A,
    INT_MOTION = 0x5F,
    INT_TAP = 0x63,
    INT_ORIENT = 0x65,
    INT_FLAT = 0x67,
    FOC_COMF = 0x69,
    COMF = 0x6A,
    IF_CONF = 0x6B,
    PMU_TRIGGER = 0x6C,
    SELF_TEST = 0x6D,
    NV_CONF = 0x70,
    OFFSET = 0x71,
    STEP_CNT = 0x78,
    STEP_CONF = 0x7A,
    CMD = 0x7E
} BMX160_Registers_t;

// Accelerometer range settings
typedef enum {
    BMX160_ACC_RANGE_2G = (0b0011),
    BMX160_ACC_RANGE_4G = (0b0101),
    BMX160_ACC_RANGE_8G = (0b1000),
    BMX160_ACC_RANGE_16G = (0b1100)
} BMX160_Acc_Range_t;

extern xQueueHandle accel_int_queue;

void BMX160_after_sleep_init();
void BMX160_full_init();

#define BMX160_write(register_address, data, length) I2C_write(BMX160_ADDRESS, register_address, data, length)
#define BMX160_write_byte(register_address, data) I2C_write_byte(BMX160_ADDRESS, register_address, data);
#define BMX160_read(register_address, data, length) I2C_read(BMX160_ADDRESS, register_address, data, length);

float BMX160_get_temperature();

#endif