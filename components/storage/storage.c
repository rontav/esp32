#define TAG "nvs storage"

#include "storage.h"

#include <esp_log.h>
#include <esp_system.h>
#include <nvs.h>
#include <nvs_flash.h>
#include <string.h>

static bool full_init = false;

nvs_handle_t handle;
Storage storage;
bool storage_dirty;

/*
 * If the NVS entry is invalid, it will be 
 * overwritten with this value.
 */
static const Storage default_storage = (Storage){
    .adsero_status = NOT_REGISTERED,
    .loaded_sim7000_certs = false,
};

void storage_after_sleep_init() {
    if (!full_init) {
        atomic_fetch_add(&init_counter, 1);
        ESP_LOGI(TAG, "after sleep init");
    }

    esp_err_t err;

    err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_LOGE(TAG, "NVS partition was truncated and needs to be erased");
        return;
    }

    ESP_LOGV(TAG, "Opening Non-Volatile Storage (NVS) handle");
    err = nvs_open(NVS_NAMESPACE, NVS_READWRITE, &handle);

    if (err != ESP_OK) {
        ESP_LOGE(TAG, "nvs_open failed: %s", esp_err_to_name(err));
        return;
    }
    ESP_LOGV(TAG, "Done");

    ESP_LOGV(TAG, "Reading saved preferences NVS");

    size_t saved_size = 0;
    err = nvs_get_blob(handle, NVS_STORAGE_ENTRY, NULL, &saved_size);

    if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGE(TAG, "Failed to get preferences size: %s", esp_err_to_name(err));
        return;
    }

    // Read previously saved preferences if available
    if (saved_size == sizeof(Storage)) {
        err = nvs_get_blob(handle, NVS_STORAGE_ENTRY, &storage, &saved_size);

        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Failed to read the preferences: %s", esp_err_to_name(err));
            return;
        }
    } else {
        // Drop saved preferences, missmatch or not saved.
        ESP_LOGW(TAG, "Missmatched saved storage size, drop and set the default preferences!");
        memcpy(&storage, &default_storage, sizeof(Storage));
        storage_save();
    }

    nvs_close(handle);

    if (!full_init) {
        atomic_fetch_add(&init_counter, -1);
        ESP_LOGI(TAG, "after sleep init finished");
    }
}
void storage_full_init() {
    full_init = true;
    atomic_fetch_add(&init_counter, 1);
    ESP_LOGI(TAG, "full init");

    storage_after_sleep_init();

    atomic_fetch_add(&init_counter, -1);
    ESP_LOGI(TAG, "full init finished");
}

bool storage_save(void) {
    esp_err_t err;

    ESP_LOGV(TAG, "Opening Non-Volatile Storage (NVS) handle...");
    err = nvs_open(NVS_NAMESPACE, NVS_READWRITE, &handle);

    if (err != ESP_OK) {
        ESP_LOGE(TAG, "nvs_open failed: %s", esp_err_to_name(err));
        return false;
    }
    ESP_LOGV(TAG, "Done");

    err = nvs_set_blob(handle, NVS_STORAGE_ENTRY, &storage, sizeof(Storage));

    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to save preferences failed: %s", esp_err_to_name(err));
        nvs_close(handle);
        return false;
    }

    err = nvs_commit(handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to commit preferences: %s", esp_err_to_name(err));
        nvs_close(handle);
        return false;
    }

    storage_dirty = false;
    nvs_close(handle);
    return true;
}
