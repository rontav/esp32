#ifndef _STORAGE_H_
#define _STORAGE_H_

#include <esp_log.h>
#include <stdint.h>

#include "SIM7000.h"
#include "utils.h"

#define NVS_NAMESPACE "Adse.ro Storage"
#define NVS_STORAGE_ENTRY "storage"

typedef enum {
    DISABLED = 0,
    NOT_REGISTERED,
    STOLEN,
    LOCKED,
    ACTIVE,
} Adsero_status;

#pragma pack(push, 1)
typedef struct {
    float adsero_voltage;
    float adsero_temperature;
    float scooter_speed;
    uint8_t battery_level;
    uint8_t power_good;
    uint8_t location_status;
    uint8_t alarm_status;
    uint8_t dashboard_status;
    uint8_t scooter_status;
    uint8_t adsero_status;
} Scooter_info;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct {
    Adsero_status adsero_status;
    Location last_sent_location;
    bool loaded_sim7000_certs;
    bool adsero_status_overwrite_server;
} Storage;
#pragma pack(pop)

void storage_after_sleep_init();
void storage_full_init();
bool storage_save(void);

extern Storage storage;
extern bool storage_dirty;

#endif