#ifndef _NJU72501_H_
#define _NJU72501_H_

#include <sdkconfig.h>

#include "utils.h"

#define DEFAULT_BUZZER_FREQUENCY 3200

void NJU72501_after_sleep_init();
void NJU72501_full_init();

typedef enum {
    BUZZER_NONE,
    BUZZER_0,
    BUZZER_1,
    BUZZER_BOTH,
} buzzer_channel;

typedef struct {
    uint32_t time_ms;
    uint32_t frequency;
    buzzer_channel channel;
} buzzer_config;

uint32_t beep(void);
uint32_t beep_ms(uint32_t);
uint32_t enqueue_beep_config(buzzer_config*);

#endif