#define TAG "NJU72501"

#include "NJU72501.h"

#include <driver/ledc.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

static bool full_init = false;

ledc_channel_config_t ledc_channel[2] = {
    {
        .channel = CONFIG_BUZZER1_CHANNEL,
        .duty = 0,
        .gpio_num = CONFIG_BUZZER1_GPIO,
        .speed_mode = LEDC_LOW_SPEED_MODE,
        .hpoint = 0,
        .timer_sel = CONFIG_BUZZER1_TIMER,
    },
    {
        .channel = CONFIG_BUZZER2_CHANNEL,
        .duty = 0,
        .gpio_num = CONFIG_BUZZER2_GPIO,
        .speed_mode = LEDC_LOW_SPEED_MODE,
        .hpoint = 0,
        .timer_sel = CONFIG_BUZZER2_TIMER,
    },
};

xQueueHandle beep_queue[2];

uint32_t beep(void) {
    return beep_ms(10);
}

uint32_t beep_ms(uint32_t time_ms) {
    buzzer_config b = {
        .channel = BUZZER_BOTH,
        .time_ms = time_ms,
        .frequency = DEFAULT_BUZZER_FREQUENCY,
    };
    return enqueue_beep_config(&b);
}

uint32_t enqueue_beep_config(buzzer_config* b) {
    uint32_t ret = 1;
    if (b->channel == BUZZER_0 || b->channel == BUZZER_BOTH) {
        atomic_fetch_add(&can_sleep, 1);
        ret &= xQueueSend(beep_queue[0], b, 10) == pdPASS ? 1 : 0;
    }
    if (b->channel == BUZZER_1 || b->channel == BUZZER_BOTH) {
        atomic_fetch_add(&can_sleep, 1);
        ret &= xQueueSend(beep_queue[1], b, 10) == pdPASS ? 1 : 0;
    }
    return ret;
}

static void alarm_task(void* arg) {
    uint8_t buzzer = *(uint8_t*)arg;
    buzzer_config b;

    for (;;) {
        if (xQueueReceive(beep_queue[buzzer], &b, portMAX_DELAY)) {
            ledc_set_freq(LEDC_LOW_SPEED_MODE, ledc_channel[buzzer].timer_sel, b.frequency);

            ledc_set_duty(LEDC_LOW_SPEED_MODE, ledc_channel[buzzer].channel, 128);
            ledc_update_duty(LEDC_LOW_SPEED_MODE, ledc_channel[buzzer].channel);

            delay(b.time_ms);

            ledc_set_duty(LEDC_LOW_SPEED_MODE, ledc_channel[buzzer].channel, 0);
            ledc_update_duty(LEDC_LOW_SPEED_MODE, ledc_channel[buzzer].channel);
            atomic_fetch_add(&can_sleep, -1);
        }
    }
}

void NJU72501_after_sleep_init() {
    if (!full_init) {
        atomic_fetch_add(&init_counter, 1);
        ESP_LOGI(TAG, "after sleep init");
    }

    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_8_BIT,
        .freq_hz = 5000,
        .speed_mode = LEDC_LOW_SPEED_MODE,
        .clk_cfg = LEDC_AUTO_CLK,
    };

    ledc_timer.timer_num = CONFIG_BUZZER1_TIMER;
    ledc_timer_config(&ledc_timer);

    ledc_timer.timer_num = CONFIG_BUZZER2_TIMER;
    ledc_timer_config(&ledc_timer);

    ledc_channel_config(&ledc_channel[0]);
    ledc_channel_config(&ledc_channel[1]);

    ledc_set_freq(LEDC_LOW_SPEED_MODE, CONFIG_BUZZER1_TIMER, 2500);
    ledc_set_freq(LEDC_LOW_SPEED_MODE, CONFIG_BUZZER2_TIMER, 2500);

    beep_queue[0] = xQueueCreate(10, sizeof(buzzer_config));
    beep_queue[1] = xQueueCreate(10, sizeof(buzzer_config));

    int buzzer0 = 0;
    int buzzer1 = 1;
    xTaskCreatePinnedToCore(alarm_task, "alarm_task_buzzer_1", 1024 * 1, &buzzer0, configMAX_PRIORITIES - 1, NULL, M365_CPU);
    xTaskCreatePinnedToCore(alarm_task, "alarm_task_buzzer_2", 1024 * 1, &buzzer1, configMAX_PRIORITIES - 1, NULL, M365_CPU);

    if (!full_init) {
        atomic_fetch_add(&init_counter, -1);
        ESP_LOGI(TAG, "after sleep init finished");
    }
}

void NJU72501_full_init() {
    full_init = true;
    atomic_fetch_add(&init_counter, 1);
    ESP_LOGI(TAG, "full init");

    NJU72501_after_sleep_init();

    atomic_fetch_add(&init_counter, -1);
    ESP_LOGI(TAG, "full init finished");
}
