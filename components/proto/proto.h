#ifndef _PROTO_H_
#define _PROTO_H_

#include <sdkconfig.h>
#include <stdint.h>

#include "M365.h"
#include "utils.h"

#define CRC0_MASK (0xff00)
#define CRC0_SHIFT (8)

#define CRC1_MASK (0x00ff)
#define CRC1_SHIFT (0)

/* Timeout connection */
#define PROTO_TIMEOUT (1000)

#define PROTO_CONSTANT (1.60934)
#define PROTO_COMMAND_HEADER0 (0x55)
#define PROTO_COMMAND_HEADER1 (0xAA)

#define PROTO_ADC_MIN (0x28)
#define PROTO_ADC_MAX (0xC2)

void proto_add_crc(uint8_t *, uint8_t);
uint8_t proto_verify_crc(uint8_t *, uint8_t);
uint16_t proto_crc(const uint8_t *, uint16_t);

void proto_command(m365_channel *);

#endif