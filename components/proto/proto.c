#define TAG "PROTO"

#include "proto.h"

#include <driver/gpio.h>
#include <esp_log.h>
#include <freertos/queue.h>

#include "adsero.h"
#include "storage.h"

uint8_t proto_verify_crc(uint8_t *message, uint8_t size) {
    uint32_t cksm = 0;
    for (uint8_t i = 2; i < size - 2; i++)
        cksm += message[i];
    cksm ^= 0xFFFF;

    if (cksm == message[size - 2] + (message[size - 1] << 8)) {
        return 1;
    }
    return 0;
}

void proto_add_crc(uint8_t *message, uint8_t size) {
    uint32_t cksm = 0;
    for (uint8_t i = 2; i < size - 2; i++)
        cksm += message[i];
    cksm ^= 0xFFFF;
    message[size - 2] = (uint8_t)(cksm & 0xFF);
    message[size - 1] = (uint8_t)((cksm & 0xFF00) >> 8);
}

uint16_t proto_crc(const uint8_t *data, uint16_t size) {
    uint32_t calc = 0;
    uint16_t ret = 0;

    for (uint8_t index = 2; index < size - 2; index++)
        calc += data[index];

    calc ^= 0xffff;
    ret |= (((uint8_t)(calc & 0xff)) << CRC0_SHIFT) & CRC0_MASK;
    ret |= (((uint8_t)((calc & 0xff00) >> 8)) << CRC1_SHIFT) & CRC1_MASK;

    return ret;
}

static uint8_t connected(m365_channel *channel) {
    /* Some way to measure the timeout, in case connection dropped */

    if (GET_TIME() - channel->last_valid_message_timestamp >= PROTO_TIMEOUT) {
        /* Something happened, reset the connection */
        return 0;
    }

    return 1;
}

static void print_stats(m365_channel *channel) {
    ESP_LOGI(TAG, "Tail: %hhu", channel->m365_protocol_data.tail);
    ESP_LOGI(TAG, "Eco: %hhu", channel->m365_protocol_data.eco);
    ESP_LOGI(TAG, "Led: %hhu", channel->m365_protocol_data.led);
    ESP_LOGI(TAG, "Night: %hhu", channel->m365_protocol_data.night);
    ESP_LOGI(TAG, "Beep: %hhu", channel->m365_protocol_data.beep);
    ESP_LOGI(TAG, "Eco: %hhu", channel->m365_protocol_data.eco_mode);
    ESP_LOGI(TAG, "Cruise: %hhu", channel->m365_protocol_data.cruise);
    ESP_LOGI(TAG, "Lock: %hhu", channel->m365_protocol_data.lock);
    ESP_LOGI(TAG, "Battery: %hhu", channel->m365_protocol_data.battery);
    ESP_LOGI(TAG, "Velocity: %hhu", channel->m365_protocol_data.velocity);
    ESP_LOGI(TAG, "AverageVelocity: %hhu", channel->m365_protocol_data.average_velocity);
    ESP_LOGI(TAG, "Odometer: %hhu", channel->m365_protocol_data.odometer);
    ESP_LOGI(TAG, "Temperature: %hhu", channel->m365_protocol_data.temperature);
}

static void process_command(m365_channel *channel, const uint8_t *command, uint16_t size) {
    /* Verify crc */
    if (!proto_verify_crc((uint8_t *)command, size))
        return;

    /* Update last valid message */
    channel->last_valid_message_timestamp = GET_TIME();
    if (channel == &m365) {
        switch (command[3]) {
            case 0x21:
                switch (command[4]) {
                    case 0x01:
                        if (command[4] == 0x61)
                            channel->m365_protocol_data.tail = command[5];
                        break;
                    case 0x64:
                        channel->m365_protocol_data.eco = command[6];
                        channel->m365_protocol_data.led = command[7];
                        channel->m365_protocol_data.night = command[8];
                        channel->m365_protocol_data.beep = command[9];
                        break;
                }
                break;
            case 0x23:
                switch (command[5]) {
                    case 0x7B:
                        channel->m365_protocol_data.eco_mode = command[6];
                        channel->m365_protocol_data.cruise = command[8];
                        break;
                    case 0x7D:
                        channel->m365_protocol_data.tail = command[6];
                        break;
                    case 0xB0:
                        channel->m365_protocol_data.alarm_status = command[8];
                        channel->m365_protocol_data.lock = command[10];
                        channel->m365_protocol_data.battery = command[14];
                        channel->m365_protocol_data.velocity = (command[16] + (command[17] * 256)) / 1000 / PROTO_CONSTANT;
                        channel->m365_protocol_data.average_velocity = (command[18] + (command[19] * 265)) / 1000 / PROTO_CONSTANT;
                        channel->m365_protocol_data.odometer = (command[20] + (command[21] * 256) + (command[22] * 256 * 256)) / 1000 / PROTO_CONSTANT;
                        channel->m365_protocol_data.temperature = ((command[28] + (command[29] * 256)) / 10 * 9 / 5) + 32;
                        break;
                }
        }

        if (channel->m365_protocol_data.beep == 1 && !channel->m365_protocol_data.alarm_status) {
            // dashboard beep
        }
        if (channel->m365_protocol_data.beep == 2) {
            // dashboard beep
            channel->m365_protocol_data.beep = 1;
        } else if (channel->m365_protocol_data.beep == 3) {
            // dashboard beep
            channel->m365_protocol_data.beep = 1;
        }

        if (channel->m365_protocol_data.alarm_status) {
            // dashboard beep
            channel->m365_protocol_data.alarm_status = 0;
        }
    } else {
        channel->m365_protocol_data.acceleration = command[7];
        channel->m365_protocol_data.brake = command[8];

        if (storage.adsero_status == ACTIVE) {
            bool condition = (dash.m365_protocol_data.acceleration == PROTO_ADC_MAX &&
                              dash.m365_protocol_data.brake == PROTO_ADC_MAX);
            if (!condition) {
                channel->m365_protocol_data.lock_step = 0;
            } else {
                if (GET_TIME() > channel->m365_protocol_data.next_check) {
                    if (channel->m365_protocol_data.lock_step == 0 ||
                        channel->m365_protocol_data.lock_step == 1 ||
                        channel->m365_protocol_data.lock_step == 2) {
                        beep_ms(300);
                        channel->m365_protocol_data.next_check = GET_TIME() + 1000;
                    } else {
                        storage.adsero_status = LOCKED;
                        storage.adsero_status_overwrite_server = true;
                        storage_save();

                        gpio_set_level(CONFIG_M365_42V_CTRL_GPIO, 0);
                        beep_ms(200);
                        delay(300);
                        beep_ms(200);
                        delay(300);
                        beep_ms(200);
                        delay(300);
                        beep_ms(200);
                        delay(300);
                        beep_ms(200);
                        delay(300);
                        gpio_set_level(CONFIG_M365_42V_CTRL_GPIO, 1);
                        gpio_set_level(CONFIG_DASH_POWER_CTRL_GPIO, 0);

                        char encoded_packet[256];
                        Location location;
                        adsero_get_location_packet(encoded_packet, &location);
                        adsero_send(encoded_packet);

                        beep_ms(100);
                        delay(200);
                        beep_ms(100);
                        delay(200);
                        esp_restart();
                    }
                    channel->m365_protocol_data.lock_step++;
                }
            }
        }
    }
}

static void process_buffer(m365_channel *channel) {
    if (channel->rx_size - channel->rx_offset <= 0 || channel->rx_size - channel->rx_offset > CONFIG_M365_BUFFER_SIZE) {
        /* Nothing to receive */
        return;
    }

    uint16_t buffer_size = channel->rx_size - channel->rx_offset;
    uint8_t *buffer = channel->rx + channel->rx_offset;

    /* Multiple commands in same buffer */
    uint8_t *command_ptr = buffer;
    uint16_t command_size = 0;

    uint16_t command_no = 0;
    uint16_t index = 0;

    while (index < buffer_size) {
        if (index &&
            buffer[index - 1] == PROTO_COMMAND_HEADER0 &&
            buffer[index] == PROTO_COMMAND_HEADER1) {
            /* Command header detected */
            if (command_no > 0) {
                /* If command_no is 0 it means that the header is for the first command */
                command_size = index - 1 - command_size;
                process_command(channel, command_ptr, command_size);
                command_ptr = buffer + index - 1;
            }
            command_no++;
        } else if (index == buffer_size - 1) {
            /* Process last packet */
            command_size = index - command_size + 1;
            process_command(channel, command_ptr, command_size);
        }
        index++;
    }
}
uint8_t i;

void proto_command(m365_channel *channel) {
    static uint8_t messageType = 0;

    m365_channel *other_channel;
    if (channel == &m365) {
        other_channel = &dash;
    } else {
        other_channel = &m365;
    }

    /* Process what's received */
    process_buffer(channel);

    if (storage.adsero_status == STOLEN) {
        if (channel == &m365) {
            if (!connected(channel)) {
                ESP_LOGD(TAG, "Not connected");
                messageType = 4;
            }

            // apply max brake if scooter is stolen
            uint8_t brake = PROTO_ADC_MAX;
            uint8_t speed = PROTO_ADC_MIN;

            /* Iterate over all message types and collect informations */
            switch (messageType++) {
                case 0:
                    /* Write speed and brake */
                case 1:
                    /* Write speed and brake */
                case 2:
                    /* Write speed and brake */
                case 3: {
                    /* I don't know what this command does, seems to be the way to actually write the speed and brake values */
                    uint8_t command[] = {0x55, 0xAA, 0x7, 0x20, 0x65, 0x0, 0x4, speed, brake, 0x0, channel->m365_protocol_data.beep, 0x0, 0x0};
                    proto_add_crc(command, sizeof(command));
                    M365_copy_tx_chan(channel, command, sizeof(command));
                    if (channel->m365_protocol_data.beep) {
                        channel->m365_protocol_data.beep = 0;
                    }
                    break;
                }
                case 4: {
                    uint8_t command[] = {0x55, 0xAA, 0x9, 0x20, 0x64, 0x0, 0x6, speed, brake, 0x0, channel->m365_protocol_data.beep, 0x72, 0x0, 0x0, 0x0};
                    proto_add_crc(command, sizeof(command));
                    M365_copy_tx_chan(channel, command, sizeof(command));
                    if (channel->m365_protocol_data.beep)
                        channel->m365_protocol_data.beep = 0;
                    break;
                }
                case 5: {
                    uint8_t command[] = {0x55, 0xAA, 0x6, 0x20, 0x61, 0xB0, 0x20, 0x02, speed, brake, 0x0, 0x0};
                    proto_add_crc(command, sizeof(command));
                    M365_copy_tx_chan(channel, command, sizeof(command));
                    break;
                }
                case 6: {
                    uint8_t command[] = {0x55, 0xAA, 0x6, 0x20, 0x61, 0x7B, 0x4, 0x2, speed, brake, 0x0, 0x0};
                    proto_add_crc(command, sizeof(command));
                    M365_copy_tx_chan(channel, command, sizeof(command));
                    break;
                }
                case 7: {
                    uint8_t command[] = {0x55, 0xAA, 0x6, 0x20, 0x61, 0x7D, 0x2, 0x2, speed, brake, 0x0, 0x0};
                    proto_add_crc(command, sizeof(command));
                    M365_copy_tx_chan(channel, command, sizeof(command));
                    messageType = 0;
                    break;
                }
            }
        }
    } else {
        if (channel->rx_size - channel->rx_offset >= 0) {
            M365_copy_tx_chan(other_channel, channel->rx + channel->rx_offset, channel->rx_size - channel->rx_offset);
        }
    }
}
