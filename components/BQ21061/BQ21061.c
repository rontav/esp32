#define TAG "BQ21061"

#include "BQ21061.h"

#include <driver/gpio.h>
#include <driver/rtc_io.h>
#include <esp_err.h>
#include <esp_log.h>
#include <esp_sleep.h>
#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

#include "I2C.h"
#include "utils.h"

static bool full_init = false;

xQueueHandle bq21061_int_queue = NULL;

static void IRAM_ATTR bq21061_int_isr_handler(void* arg) {
    if (gpio_get_level(CONFIG_POWER_GOOD_GPIO) == 0) {
        atomic_fetch_add(&can_sleep, 1);
    } else {
        atomic_fetch_add(&can_sleep, -1);
    }
    uint8_t data = 1;
    xQueueSendFromISR(bq21061_int_queue, &data, NULL);
}

static void bq21061_int_task(void* arg) {
    uint8_t data;
    for (;;) {
        if (xQueueReceive(bq21061_int_queue, &data, portMAX_DELAY)) {
            BQ21061_setup();
        }
    }
}

void BQ21061_setup() {
    BQ21061_write_byte(ICHG_CTRL, (CONFIG_MAX_CHARGE_CURRENT_mA / 2.5));
    BQ21061_write_byte(PCHRGCTRL, 0b10000100);     // charge step to 2.5mA; pre-charge current to 10mA
    BQ21061_write_byte(TERMCTRL, 0b00011000);      // CONFIG_MAX_CHARGE_CURRENT_mA * 12% (50mA termination current if CONFIG_MAX_CHARGE_CURRENT_mA = 400mA)
    BQ21061_write_byte(BUVLO, 0b00001100);         // 3.0V fast charge; 1.5A battery current protection; 2.6V battery uvlo
    BQ21061_write_byte(CHARGERCTRL0, 0b10010100);  // allow up to 12h fast charge; disable i2c watchdog
    BQ21061_write_byte(CHARGERCTRL1, 0b01100000);  // enable vin dynamic power management and set it to 4.8V; set chip thermal current foldback threshold at 80°C
    BQ21061_write_byte(LDOCTRL, 0b00110000);       // disable LDO
    BQ21061_write_byte(ICCTRL2, 0b00000010);       // set PMID to be the same as battery voltage even if charging; enable vin startup watchdog
}

void BQ21061_after_sleep_init() {
    if (!full_init) {
        atomic_fetch_add(&init_counter, 1);
        ESP_LOGI(TAG, "after sleep init");
    }

    rtc_gpio_deinit(CONFIG_POWER_GOOD_GPIO);
    gpio_reset_pin(CONFIG_POWER_GOOD_GPIO);
    gpio_set_direction(CONFIG_POWER_GOOD_GPIO, GPIO_MODE_INPUT);

    bq21061_int_queue = xQueueCreate(2, sizeof(uint8_t));
    xTaskCreatePinnedToCore(bq21061_int_task, "bq21061_int_task", 1024, NULL, configMAX_PRIORITIES - 1, NULL, APP_CPU);

    gpio_set_intr_type(CONFIG_POWER_GOOD_GPIO, GPIO_INTR_ANYEDGE);
    gpio_isr_handler_add(CONFIG_POWER_GOOD_GPIO, bq21061_int_isr_handler, NULL);
    gpio_intr_enable(CONFIG_POWER_GOOD_GPIO);

    if (gpio_get_level(CONFIG_POWER_GOOD_GPIO) == 0) {
        atomic_fetch_add(&can_sleep, 1);
        BQ21061_setup();
    }

    if (!full_init) {
        atomic_fetch_add(&init_counter, -1);
        ESP_LOGI(TAG, "after sleep init finished");
    }
}

void BQ21061_full_init() {
    full_init = true;
    atomic_fetch_add(&init_counter, 1);
    ESP_LOGI(TAG, "full init");

    BQ21061_after_sleep_init();

    atomic_fetch_add(&init_counter, -1);
    ESP_LOGI(TAG, "full init finished");
}
