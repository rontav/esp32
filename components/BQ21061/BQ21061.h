#ifndef _BQ21061_H_
#define _BQ21061_H_

#include <sdkconfig.h>

#include "utils.h"

// Register addresses
typedef enum {
    STAT0 = 0x0,
    STAT1 = 0x1,
    STAT2 = 0x2,
    FLAG0 = 0x3,
    FLAG1 = 0x4,
    FLAG2 = 0x5,
    FLAG3 = 0x6,
    MASK0 = 0x7,
    MASK1 = 0x8,
    MASK2 = 0x9,
    MASK3 = 0xA,
    VBAT_CTRL = 0x12,
    ICHG_CTRL = 0x13,
    PCHRGCTRL = 0x14,
    TERMCTRL = 0x15,
    BUVLO = 0x16,
    CHARGERCTRL0 = 0x17,
    CHARGERCTRL1 = 0x18,
    ILIMCTRL = 0x19,
    LDOCTRL = 0x1D,
    MRCTRL = 0x30,
    ICCTRL0 = 0x35,
    ICCTRL1 = 0x36,
    ICCTRL2 = 0x37,
    TS_FASTCHGCTRL = 0x61,
    TS_COLD = 0x62,
    TS_COOL = 0x63,
    TS_WARM = 0x64,
    TS_HOT = 0x65,
    DEVICE_ID = 0x6F,  // should return 0x3A
} BQ21061_Registers_t;

void BQ21061_setup();
void BQ21061_after_sleep_init();
void BQ21061_full_init();

#define BQ21061_write(register_address, data, length) I2C_write(CONFIG_BQ21061_ADDRESS, register_address, data, length)
#define BQ21061_write_byte(register_address, data) I2C_write_byte(CONFIG_BQ21061_ADDRESS, register_address, data);
#define BQ21061_read(register_address, data, length) I2C_read(CONFIG_BQ21061_ADDRESS, register_address, data, length);

float BQ21061_get_temperature();

#endif