#define TAG "I2C"

#include "I2C.h"

#include <driver/i2c.h>
#include <esp_log.h>

#include "utils.h"

static bool full_init = false;

esp_err_t I2C_write(uint8_t device_address, uint8_t register_address, void* data, uint8_t length) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (device_address << 1) | I2C_MASTER_WRITE, 1);
    i2c_master_write_byte(cmd, register_address, 1);

    i2c_master_write(cmd, data, length, 1);
    i2c_master_stop(cmd);

    esp_err_t espRc = i2c_master_cmd_begin(I2C_NUM_0, cmd, 10 / portTICK_PERIOD_MS);

    i2c_cmd_link_delete(cmd);

    return espRc;
}

esp_err_t I2C_write_byte(uint8_t device_address, uint8_t register_address, uint8_t data) {
    return I2C_write(device_address, register_address, &data, 1);
}

esp_err_t I2C_read(uint8_t device_address, uint8_t register_address, void* data, uint8_t length) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (device_address << 1) | I2C_MASTER_WRITE, 1);
    i2c_master_write_byte(cmd, register_address, 1);

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (device_address << 1) | I2C_MASTER_READ, 1);

    if (length > 1) {
        i2c_master_read(cmd, data, length - 1, I2C_MASTER_ACK);
    }
    i2c_master_read_byte(cmd, data + length - 1, I2C_MASTER_NACK);
    i2c_master_stop(cmd);

    esp_err_t espRc = i2c_master_cmd_begin(I2C_NUM_0, cmd, 10 / portTICK_PERIOD_MS);

    i2c_cmd_link_delete(cmd);

    return espRc;
}

void I2C_after_sleep_init() {
    if (!full_init) {
        atomic_fetch_add(&init_counter, 1);
        ESP_LOGI(TAG, "after sleep init");
    }

    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = SDA_PIN;
    conf.scl_io_num = SCL_PIN;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;
    conf.master.clk_speed = SCL_SPEED;
    i2c_param_config(I2C_NUM_0, &conf);

    i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0);

    if (!full_init) {
        atomic_fetch_add(&init_counter, -1);
        ESP_LOGI(TAG, "after sleep init finished");
    }
}

void I2C_full_init() {
    full_init = true;
    atomic_fetch_add(&init_counter, 1);
    ESP_LOGI(TAG, "full init");

    I2C_after_sleep_init();

    atomic_fetch_add(&init_counter, -1);
    ESP_LOGI(TAG, "full init finished");
}
