#ifndef _I2C_H_
#define _I2C_H_

#include <esp_err.h>
#include <sdkconfig.h>
#include <stdint.h>

#define SDA_PIN (CONFIG_I2C_SDA_GPIO)
#define SCL_PIN (CONFIG_I2C_SCL_GPIO)
#define SCL_SPEED 400000

void I2C_after_sleep_init();
void I2C_full_init();

esp_err_t I2C_write(uint8_t device_address, uint8_t register_address, void* data, uint8_t length);
esp_err_t I2C_write_byte(uint8_t device_address, uint8_t register_address, uint8_t data);
esp_err_t I2C_read(uint8_t device_address, uint8_t register_address, void* data, uint8_t length);

#endif