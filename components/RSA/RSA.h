#ifndef _RSA_H_
#define _RSA_H_

#include <mbedtls/pk.h>
#include <mbedtls/rsa.h>
#include <mbedtls/x509_crt.h>

#include "utils.h"

extern mbedtls_x509_crt crt;
extern mbedtls_rsa_context* rsa;

void RSA_after_sleep_init();
void RSA_full_init();
int rsa_decrypt(const uint8_t*, uint8_t*);

#endif