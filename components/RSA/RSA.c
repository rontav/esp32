#define TAG "RSA"

#include "RSA.h"

#include <esp_log.h>
#include <string.h>

#include "cert.h"

mbedtls_x509_crt crt;
mbedtls_rsa_context* rsa;

static bool full_init = false;

int rsa_decrypt(const uint8_t* encrypted_message, uint8_t* decrypted_message) {
    int ret;
    size_t len = 0;

    bzero(decrypted_message, ENC_LEN_BYTE);

    ESP_LOGV(TAG, "Encrypted Message:");
    ESP_LOG_BUFFER_HEXDUMP(TAG, encrypted_message, ENC_LEN_BYTE, ESP_LOG_VERBOSE);

    ret = mbedtls_rsa_pkcs1_decrypt(rsa, NULL, NULL, MBEDTLS_RSA_PUBLIC, &len, encrypted_message, decrypted_message, ENC_LEN_BYTE);

    if (ret == MBEDTLS_ERR_RSA_INVALID_PADDING) {
        ret = mbedtls_rsa_public(rsa, encrypted_message, decrypted_message);
        len = ENC_LEN_BYTE;
    }

    ESP_LOGV(TAG, "Decrypted Message:");
    ESP_LOG_BUFFER_HEXDUMP(TAG, decrypted_message, ENC_LEN_BYTE, ESP_LOG_VERBOSE);

    return ret;
}

void RSA_after_sleep_init() {
    if (!full_init) {
        atomic_fetch_add(&init_counter, 1);
        ESP_LOGI(TAG, "after sleep init");
    }

    mbedtls_x509_crt_init(&crt);
    mbedtls_x509_crt_parse(&crt, (const uint8_t*)SERVER_CERT, strlen(SERVER_CERT) + 1);

    rsa = mbedtls_pk_rsa(crt.pk);

    mbedtls_rsa_check_pubkey(rsa);
    mbedtls_rsa_set_padding(rsa, MBEDTLS_RSA_PKCS_V15, crt.sig_md);

    if (!full_init) {
        atomic_fetch_add(&init_counter, -1);
        ESP_LOGI(TAG, "after sleep init finished");
    }
}

void RSA_full_init() {
    full_init = true;
    atomic_fetch_add(&init_counter, 1);
    ESP_LOGI(TAG, "full init");

    RSA_after_sleep_init();

    atomic_fetch_add(&init_counter, -1);
    ESP_LOGI(TAG, "full init finished");
}
