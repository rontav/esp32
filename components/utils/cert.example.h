#ifndef _CERT_H_
#define _CERT_H_

#define ENC_LEN_BIT 2048
#define ENC_LEN_BYTE (ENC_LEN_BIT / 8)

#define SERVER_CERT "server certificate"

#define CLIENT_CERT "client certificate"
// client private key must be unencrypted RSA1024/RSA2048 to work with SIM7000
#define CLIENT_KEY "client private key"

#endif