#define TAG "utils"

#include "utils.h"

#include <esp_log.h>
#include <esp_sleep.h>
#include <string.h>

#include "SIM7000.h"

void decode_hex_in_place(char* str) {
    int length = strlen(str);

    for (int i = 0; i < length; i += 2) {
        int digit_1, digit_2;

        digit_1 = parse_hex_char(str[i]);
        digit_2 = parse_hex_char(str[i + 1]);

        str[i / 2] = digit_1 * 16 + digit_2;
    }
    str[length / 2] = '\0';
}

uint8_t parse_hex_char(char c) {
    if (c >= '0' && c <= '9') {
        return c - '0';
    }
    if (c >= 'A' && c <= 'F') {
        return c - 'A' + 10;
    }
    if (c >= 'a' && c <= 'f') {
        return c - 'a' + 10;
    }
    return 0;
}
