#ifndef _UTILS_H_
#define _UTILS_H_

#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>
#include <sdkconfig.h>
#include <stdatomic.h>
#include <stdint.h>
#include <time.h>

#include "NJU72501.h"

#define M365_CPU 1
#define APP_CPU 0

/* Get current time in ms */
#define GET_TIME() ((uint32_t)(clock() * 1000 / CLOCKS_PER_SEC))

#define delay(ms) (vTaskDelay((ms) / portTICK_PERIOD_MS))

#define ULP_TIMER_PERIOD_MS 10

extern atomic_int init_counter;
extern atomic_int can_sleep;

void decode_hex_in_place(char*);
uint8_t parse_hex_char(char);

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c %c%c%c%c"
#define BYTE_TO_BINARY(byte) (byte & 0x80 ? '1' : '0'), \
                             (byte & 0x40 ? '1' : '0'), \
                             (byte & 0x20 ? '1' : '0'), \
                             (byte & 0x10 ? '1' : '0'), \
                             (byte & 0x08 ? '1' : '0'), \
                             (byte & 0x04 ? '1' : '0'), \
                             (byte & 0x02 ? '1' : '0'), \
                             (byte & 0x01 ? '1' : '0')

typedef struct {
    uint8_t alarm_status;
    uint8_t average_velocity;
    uint8_t battery;
    uint8_t beep;
    uint8_t brake_connected;
    uint8_t cruise;
    uint8_t eco;
    uint8_t eco_mode;
    uint8_t has_connected;
    uint8_t is_connected;
    uint8_t led;
    uint8_t lock;
    uint8_t night;
    uint8_t odometer;
    uint8_t tail;
    uint8_t temperature;
    uint8_t throttle_connected;
    uint8_t velocity;
    uint8_t acceleration;
    uint8_t brake;
    uint8_t lock_step;
    uint32_t next_check;
} proto_stat;

#endif