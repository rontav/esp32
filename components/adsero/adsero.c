#define TAG "ADSERO"

#include "adsero.h"

#include <driver/gpio.h>
#include <esp_err.h>
#include <esp_log.h>
#include <esp_sleep.h>
#include <math.h>
#include <mbedtls/base64.h>
#include <stdint.h>
#include <string.h>

#include "BMX160.h"
#include "M365.h"
#include "SIM7000.h"
#include "bluetooth.h"
#include "proto.h"
#include "storage.h"

#define ADSERO_BUFFER_SIZE 128

static bool full_init = false;

char adsero_buffer[ADSERO_BUFFER_SIZE];

bool connected_to_network = false;
bool connected_to_server = false;
bool bluetooth_active = false;

SemaphoreHandle_t registered_sem;

esp_err_t server_connect(uint32_t timeout_ms) {
    ESP_LOGI(TAG, "Connecting to server...");
    // close existing connection
    SIM7000_send("AT+CACLOSE=0");

    // connect to APN
    if (SIM7000_expect_nonstrict("AT+CGDCONT?", "1,\"IP\",\"" CONFIG_SIM_APN_NAME "\"") != ESP_OK) {
        SIM7000_expect_ok("AT+CGDCONT=1,\"IP\",\"" CONFIG_SIM_APN_NAME "\"");
    }
    if (SIM7000_expect_nonstrict("AT+CNACT?", "+CNACT: 1") != ESP_OK) {
        SIM7000_send("AT+CNACT=1,\"" CONFIG_SIM_APN_NAME "\"");
    }
    if (SIM7000_expect_nonstrict("AT+CSTT?", "+CSTT: \"" CONFIG_SIM_APN_NAME "\"") != ESP_OK) {
        SIM7000_expect_ok("AT+CSTT=\"" CONFIG_SIM_APN_NAME "\"");
    }
    // wait for IP
    SIM7000_generic_send("AT+CIICR", 85000);
    SIM7000_generic_send("AT+CIFSR", 10000);

    // activate SSL context 0
    if (SIM7000_expect_nonstrict("AT+CACID?", "+CACID: 0") != ESP_OK) {
        SIM7000_send_expect("AT+CACID=0", 5000, "OK");
    }

    // configure SSL
    SIM7000_expect_ok("AT+CASSLCFG=0,\"ssl\",1");
    SIM7000_expect_ok("AT+CASSLCFG=0,\"protocol\",0");
    SIM7000_expect_ok("AT+CASSLCFG=0,\"cacert\",\"server.pem\"");
    SIM7000_expect_ok("AT+CASSLCFG=0,\"clientcert\",\"client.pem\"");

    // open socket
    SIM7000_generic_send("AT+CAOPEN=0,\"adse.ro\",\"444\"", 160000);
    char* ptr = strstr(sim7000_buffer, "+CAOPEN: 0,0");

    if (ptr == NULL) {
        SIM7000_send_expect("AT+CIPSHUT", 65000, "SHUT OK");
        connected_to_network = false;
        connected_to_server = false;
        return ESP_FAIL;
    } else {
        return ESP_OK;
    }
}

esp_err_t server_send(char* packet, uint32_t timeout_ms) {
    char temp[32];
    sprintf(temp, "AT+CASEND=0,%d", strlen(packet));

    SIM7000_send(temp);
    SIM7000_send(packet);
    SIM7000_wait_flush();

    server_recv(temp, 32, timeout_ms);

    char* ptr = strstr(temp, "ack");
    if (ptr == NULL) {
        ESP_LOGE(TAG, "Unexpected response to write: %s", sim7000_buffer);
        return ESP_FAIL;
    } else {
        SIM7000_wait_flush();
        return ESP_OK;
    }
}

esp_err_t server_recv(char* output, uint32_t length, uint32_t timeout_ms) {
    char recv[32];
    sprintf(recv, "AT+CARECV=0,%d", length);

    uint32_t end_time = GET_TIME() + timeout_ms;
    do {
        if (GET_TIME() > end_time) {
            break;
        }

        SIM7000_send(recv);

        char* ptr = strstr(sim7000_buffer, "+CARECV: ");
        if (ptr == NULL) {
            delay(100);
            continue;
        }

        ptr += strlen("+CARECV: ");
        if (ptr[0] == '0') {
            delay(100);
            continue;
        }

        ptr = strchr(ptr, ',') + 1;

        strncpy(output, ptr, length);
        return ESP_OK;
    } while (1);

    return ESP_ERR_TIMEOUT;
}

esp_err_t adsero_send(char* packet) {
    while (xSemaphoreTake(sim7000_mutex, portMAX_DELAY) != pdTRUE) {
        delay(500);
    }

    if (!connected_to_network) {
        connected_to_network = SIM7000_connect_network() == ESP_OK;
    }

    if (!connected_to_server) {
        if (server_connect(30000) != ESP_OK) {
            ESP_LOGE(TAG, "Couldn't connect to TCP server");
            xSemaphoreGive(sim7000_mutex);
            return ESP_FAIL;
        }
        ESP_LOGI(TAG, "Connected");
        connected_to_server = true;
    }

    esp_err_t err = server_send(packet, 10000);
    if (err != ESP_OK) {
        connected_to_server = false;
        xSemaphoreGive(sim7000_mutex);
        return err;
    }

    SIM7000_wait_flush();
    xSemaphoreGive(sim7000_mutex);

    return ESP_OK;
}

uint64_t base64(char* input, size_t length, char* output) {
    ESP_LOGV(TAG, "Original Packet %d:", length);
    ESP_LOG_BUFFER_HEXDUMP(TAG, input, length, ESP_LOG_VERBOSE);

    size_t bytesWritten = 0;

    mbedtls_base64_encode(
        (unsigned char*)output,
        256 - 1,
        &bytesWritten,
        (unsigned char*)input,
        length);

    ESP_LOGV(TAG, "Encoded packet %d:", bytesWritten);
    ESP_LOG_BUFFER_HEXDUMP(TAG, output, bytesWritten, ESP_LOG_VERBOSE);

    output[bytesWritten] = '\n';
    output[bytesWritten + 1] = '\0';

    return bytesWritten;
}

void adsero_get_location_packet(char* output, Location* location) {
    uint16_t length = 1 + 1 + sizeof(Location) + sizeof(Scooter_info);
    char packet[length];
    memset(packet, 0, length);
    packet[0] = LOCATION_MUTATION_ID;
    packet[1] = storage.adsero_status_overwrite_server;

    esp_err_t err = SIM7000_get_location(location, 1000);

    if (err == ESP_OK) {
        memcpy(packet + 2, location, sizeof(Location));

        SIM7000_print_location(location);
    }

    Scooter_info info;
    memset(&info, 0, sizeof(Scooter_info));

    while (xSemaphoreTake(sim7000_mutex, portMAX_DELAY) != pdTRUE) {
        delay(500);
    }
    SIM7000_send("AT+CBC");
    char* ptr = strstr(sim7000_buffer, "+CBC: 0,") + strlen("+CBC: 0,");

    strtol(ptr, &ptr, 10);
    ptr++;
    info.adsero_status = storage.adsero_status;
    info.adsero_voltage = strtol(ptr, &ptr, 10) / 1000.0;
    info.adsero_temperature = BMX160_get_temperature();
    info.scooter_speed = m365.m365_protocol_data.velocity * 1.60934;
    info.battery_level = m365.m365_protocol_data.battery;
    info.power_good = gpio_get_level(CONFIG_POWER_GOOD_GPIO) == 0;
    info.location_status = 1;
    memcpy(packet + 2 + sizeof(Location), &info, sizeof(Scooter_info));

    xSemaphoreGive(sim7000_mutex);

    base64(packet, length, output);
}

bool adsero_set_status(Adsero_status new_status) {
    storage.adsero_status = new_status;
    storage.adsero_status_overwrite_server = true;

    storage_save();

    delay(500);
    esp_restart();
    return true;
}

void adsero_get_status_from_server() {
    SIM7000_wait_flush();

    char encoded_packet[256];
    char packet = SCOOTER_STATUS_QUERY_ID;

    base64(&packet, 1, encoded_packet);
    adsero_send(encoded_packet);

    char temp[2];
    server_recv(temp, 2, 5000);

    if (temp[0] >= '0' && temp[0] <= '0' + ACTIVE) {
        int t = temp[0] - '0';

        if (storage.adsero_status_overwrite_server) {
            if (t == storage.adsero_status) {
                storage.adsero_status_overwrite_server = false;
                storage_save();
            }
        } else if (t != storage.adsero_status) {
            storage.adsero_status = t;
            storage_save();

            delay(500);
            esp_restart();
        }
    }
}

void adsero_register(uint32_t user_dbid) {
    char encoded_packet[256];
    char packet[1 + sizeof(user_dbid)];
    memset(packet, 0, 1 + sizeof(user_dbid));
    packet[0] = REGISTER_MUTATION_ID;
    memcpy(packet + 1, &user_dbid, sizeof(user_dbid));

    base64(packet, 1 + sizeof(user_dbid), encoded_packet);
    adsero_send(encoded_packet);

    delay(500);
    adsero_get_status_from_server();
}

// Haversine formula
double diff_coords(Location* l1, Location* l2) {
    double R = 6378.137;  // Radius of earth in KM

    double dLat = l2->latitude * M_PI / 180 - l1->latitude * M_PI / 180;
    double dLon = l2->longitude * M_PI / 180 - l1->longitude * M_PI / 180;

    double a = sin(dLat / 2) * sin(dLat / 2) +
               cos(l1->latitude * M_PI / 180) * cos(l2->latitude * M_PI / 180) *
                   sin(dLon / 2) * sin(dLon / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double d = R * c;
    return d * 1000;  // meters
}

static void adsero_location_tracker(void* arg) {
    ESP_LOGW(TAG, "adsero status: %d", storage.adsero_status);

    uint32_t start_time;
    char encoded_packet[256];
    Location location;

    while (1) {
        if (storage.adsero_status == STOLEN) {
            // wait 10s actively waiting for location
            // then go to light sleep for the rest of wait duration
            esp_err_t err = SIM7000_get_location(&location, 10 * 1000);
            if (err == ESP_FAIL) {
                SIM7000_activate_location();
                continue;
            } else if (err == ESP_ERR_TIMEOUT) {
                esp_sleep_enable_timer_wakeup((STOLEN_TIME_WAIT_FOR_LOCATION - 10 * 1000) * 1000);
                esp_light_sleep_start();
            }
            err = SIM7000_get_location(&location, 10 * 1000);

            SIM7000_send("AT+CBC");
            char* ptr = strstr(sim7000_buffer, "+CBC: 0,") + strlen("+CBC: 0,");

            uint8_t battery_level = strtol(ptr, &ptr, 10);
            if (battery_level <= 50 || battery_level > 100) {
                // power on M365 to charge the battery
                gpio_set_level(CONFIG_M365_42V_CTRL_GPIO, 1);
                delay(500);
                gpio_set_level(CONFIG_M365_42V_CTRL_GPIO, 0);
            }

            if (err == ESP_OK && (storage.last_sent_location.scooter_timestamp == 0 ||
                                  diff_coords(&storage.last_sent_location, &location) > 50 ||
                                  location.scooter_timestamp - storage.last_sent_location.scooter_timestamp > STOLEN_TIME_BETWEEN_SAME_LOCATION_UPDATE / 1000)) {
                adsero_get_location_packet(encoded_packet, &location);
                SIM7000_deactivate_location();

                if (adsero_send(encoded_packet) == ESP_OK) {
                    memcpy(&storage.last_sent_location, &location, sizeof(Location));
                    storage_save();
                    adsero_get_status_from_server();
                }
            }

            esp_sleep_enable_timer_wakeup(STOLEN_TIME_BETWEEN_LOCATION_UPDATE * 1000);
            atomic_fetch_add(&can_sleep, -1);
            delay(30 * 1000);
        } else if (storage.adsero_status == LOCKED) {
            adsero_get_status_from_server();
            adsero_get_location_packet(encoded_packet, &location);
            adsero_send(encoded_packet);

            esp_sleep_enable_timer_wakeup(LOCKED_TIME_BETWEEN_STATUS_UPDATE * 1000);
            atomic_fetch_add(&can_sleep, -1);
            delay(30 * 1000);
        } else if (storage.adsero_status == NOT_REGISTERED) {
            adsero_get_status_from_server();

            // not yet registered
            xSemaphoreTake(registered_sem, portMAX_DELAY);
        } else if (storage.adsero_status == ACTIVE) {
            start_time = GET_TIME();

            adsero_get_location_packet(encoded_packet, &location);
            adsero_send(encoded_packet);

            uint32_t temp = GET_TIME() - start_time;
            if (temp < TIME_BETWEEN_LOCATION_UPDATE_NOTLOCKED) {
                delay(TIME_BETWEEN_LOCATION_UPDATE_NOTLOCKED - temp);
            }
        } else {
            storage.adsero_status_overwrite_server = false;
            storage_save();
            adsero_get_status_from_server();
        }
    }
}

void adsero_after_sleep_init() {
    if (!full_init) {
        atomic_fetch_add(&init_counter, 1);
        ESP_LOGI(TAG, "after sleep init");
    }

    atomic_fetch_add(&can_sleep, 1);

    if (storage.adsero_status == NOT_REGISTERED) {
        bluetooth_active = true;
        registered_sem = xSemaphoreCreateBinary();
        bluetooth_init();
    }
    xTaskCreatePinnedToCore(adsero_location_tracker, "adsero_location_tracker", 1024 * 8, NULL, 0, NULL, APP_CPU);

    if (!full_init) {
        atomic_fetch_add(&init_counter, -1);
        ESP_LOGI(TAG, "after sleep init finished");
    }
}

void adsero_full_init() {
    full_init = true;
    atomic_fetch_add(&init_counter, 1);
    ESP_LOGI(TAG, "full init");

    adsero_after_sleep_init();

    atomic_fetch_add(&init_counter, -1);
    ESP_LOGI(TAG, "full init finished");
}