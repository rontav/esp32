#ifndef _ADSERO_H_
#define _ADSERO_H_

#include <esp_err.h>
#include <sdkconfig.h>

#include "storage.h"
#include "utils.h"

#define LOCATION_MUTATION_ID 'L'
#define INFO_MUTATION_ID 'I'
#define BLUETOOTH_MUTATION_ID 'B'
#define SCOOTER_STATUS_QUERY_ID 'S'
#define REGISTER_MUTATION_ID 'R'

#define TIME_BETWEEN_LOCATION_UPDATE_NOTLOCKED (5 * 1000)
#define LOCKED_TIME_BETWEEN_STATUS_UPDATE (6 * 60 * 60 * 1000LL)                             // update vehicle status every 6 hours
#define STOLEN_TIME_BETWEEN_LOCATION_UPDATE (5 * 60 * 1000)                                  // check location every 5 minutes
#define STOLEN_TIME_BETWEEN_SAME_LOCATION_UPDATE (12 * STOLEN_TIME_BETWEEN_LOCATION_UPDATE)  // if the location is the same with the last one, resend it after this many ms
#define STOLEN_TIME_WAIT_FOR_LOCATION (40 * 1000)                                            // time to wait for aquiring location

void adsero_after_sleep_init();
void adsero_full_init();

esp_err_t server_connect();
esp_err_t server_auth(uint32_t);
esp_err_t server_send(char*, uint32_t);
int server_recv(char*, uint32_t, uint32_t);
esp_err_t adsero_send(char* packet);
void adsero_register(uint32_t user_dbid);

bool adsero_set_status(Adsero_status new_status);
void adsero_get_location_packet(char* output, Location* location);

#endif