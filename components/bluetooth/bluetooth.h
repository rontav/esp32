#ifndef _BLUETOOTH_H_
#define _BLUETOOTH_H_

#include <assert.h>
#include <console/console.h>
#include <esp_bt.h>
#include <esp_log.h>
#include <esp_nimble_hci.h>
#include <host/ble_hs.h>
#include <host/ble_uuid.h>
#include <host/util/util.h>
#include <nimble/nimble_port.h>
#include <nimble/nimble_port_freertos.h>
#include <sdkconfig.h>
#include <services/gap/ble_svc_gap.h>
#include <services/gatt/ble_svc_gatt.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"

#define GATT_USER_DATA_UUID 0x181C
#define GATT_USER_DBID_UUID 0x2AC3
#define GATT_REGISTER_STATUS_UUID 0x2AC4

#define GATT_DEVICE_INFO_UUID 0x180A
#define GATT_MANUFACTURER_NAME_UUID 0x2A29
#define GATT_MODEL_NUMBER_UUID 0x2A24

void bluetooth_init(void);

#endif