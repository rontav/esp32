/* ULP assembly files are passed through C preprocessor first, so include directives
   and C macros may be used in these files 
 */
#include "soc/rtc_cntl_reg.h"
#include "soc/rtc_io_reg.h"
#include "soc/soc_ulp.h"

	/* zero-initialized variables */
	.bss
	.global wakeup_reason
wakeup_reason:
	.long 0

	.global power_good_gpio
power_good_gpio:
	.long 0
	.global accel_int_gpio
accel_int_gpio:
	.long 0
	.global sim7000_ri_gpio
sim7000_ri_gpio:
	.long 0

	/* Code goes into .text section */
	.text
read_io:
	/* Lower 16 IOs and higher need to be handled separately,
	 * because r0-r3 registers are 16 bit wide.
	 * Check which IO this is.
	 */
	move r0, r3
	jumpr read_io_high, 16, ge

	/* Read the value of lower 16 RTC IOs into R0 */
	READ_RTC_REG(RTC_GPIO_IN_REG, RTC_GPIO_IN_NEXT_S, 16)
	rsh r0, r0, r3
	jump read_done

	/* Read the value of RTC IOs 16-17, into R0 */
read_io_high:
	READ_RTC_REG(RTC_GPIO_IN_REG, RTC_GPIO_IN_NEXT_S + 16, 2)
	sub r3, r3, 16
	rsh r0, r0, r3

read_done:
	jump r1

	.global entry
entry:
	/* enable ulp timer wake-up */
	WRITE_RTC_REG(RTC_CNTL_STATE0_REG, RTC_CNTL_ULP_CP_SLP_TIMER_EN_S, 1, 1)

	/* Read power_good_gpio */
	move r3, power_good_gpio
	ld r3, r3, 0
	move r1, power_good_read_done
	jump read_io
power_good_read_done:
	and r0, r0, 1
	/* if PG GPIO is 0, wakeup */
	jump wake_up_power_good, EQ

	/* Read accel_int_gpio */
	move r3, accel_int_gpio
	ld r3, r3, 0
	move r1, accel_int_gpio_read_done
	jump read_io
accel_int_gpio_read_done:
	and r0, r0, 1
	/* if accel_int_gpio is 0, wakeup */
	jump wake_up_accel_int, EQ

	/* Read accel_int_gpio */
	move r3, sim7000_ri_gpio
	ld r3, r3, 0
	move r1, sim7000_ri_gpio_read_done
	jump read_io
sim7000_ri_gpio_read_done:
	and r0, r0, 1
	/* if sim7000_ri_gpio is 0, wakeup */
	jump wake_up_sim7000, EQ

	halt

wake_up_power_good:
	move r0, 1
	jump set_wakeup_reason
wake_up_accel_int:
	move r0, 2
	jump set_wakeup_reason
wake_up_sim7000:
	move r0, 3
	jump set_wakeup_reason

set_wakeup_reason:
	move r1, wakeup_reason
	st r0, r1, 0
wake_up:
	/* Check if the system can be woken up */
	READ_RTC_FIELD(RTC_CNTL_LOW_POWER_ST_REG, RTC_CNTL_RDY_FOR_WAKEUP)
	and r0, r0, 1
	jump wake_up, eq

	/* disable ULP timer and wake up the main processor */
	WRITE_RTC_FIELD(RTC_CNTL_STATE0_REG, RTC_CNTL_ULP_CP_SLP_TIMER_EN, 0)
	wake
	halt