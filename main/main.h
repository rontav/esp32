#ifndef _MAIN_H_
#define _MAIN_H_

typedef enum {
    NOT_BY_ULP = 0,
    POWER_GOOD,
    ACCEL_INT,
    SIM7000,
    TIMER,
} ULP_wakeup_reason;

#endif