#define TAG "main"

#include "main.h"

#include <driver/adc.h>
#include <driver/gpio.h>
#include <driver/rtc_io.h>
#include <esp32/ulp.h>
#include <esp_log.h>
#include <esp_sleep.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <soc/rtc.h>
#include <soc/rtc_cntl_reg.h>
#include <soc/rtc_periph.h>
#include <soc/sens_reg.h>
#include <stdio.h>

#include "BMX160.h"
#include "BQ21061.h"
#include "I2C.h"
#include "M365.h"
#include "NJU72501.h"
#include "RSA.h"
#include "SIM7000.h"
#include "adsero.h"
#include "storage.h"
#include "ulp_main.h"
#include "utils.h"

extern const uint8_t ulp_main_bin_start[] asm("_binary_ulp_main_bin_start");
extern const uint8_t ulp_main_bin_end[] asm("_binary_ulp_main_bin_end");

atomic_int init_counter;
atomic_int can_sleep;

static bool full_init = false;
bool needs_full_init;

void config_rtcio_input(gpio_num_t gpio) {
    /* Initialize selected GPIO as RTC IO input; disable pullup and pulldown */
    rtc_gpio_init(gpio);
    rtc_gpio_set_direction(gpio, RTC_GPIO_MODE_INPUT_ONLY);
    rtc_gpio_pulldown_dis(gpio);
    rtc_gpio_pullup_dis(gpio);
    rtc_gpio_hold_en(gpio);
}

void ulp_init() {
    ESP_ERROR_CHECK(ulp_load_binary(0, ulp_main_bin_start,
                                    (ulp_main_bin_end - ulp_main_bin_start) / sizeof(uint32_t)));

    ulp_power_good_gpio = rtc_gpio_desc[CONFIG_POWER_GOOD_GPIO].rtc_num;
    ulp_accel_int_gpio = rtc_gpio_desc[CONFIG_ACCEL_INT_GPIO].rtc_num;
    ulp_sim7000_ri_gpio = rtc_gpio_desc[CONFIG_SIM7000_RI_GPIO].rtc_num;

    ulp_set_wakeup_period(0, ULP_TIMER_PERIOD_MS * 1000);
}

void ulp_start() {
    config_rtcio_input(CONFIG_POWER_GOOD_GPIO);
    config_rtcio_input(CONFIG_ACCEL_INT_GPIO);
    config_rtcio_input(CONFIG_SIM7000_RI_GPIO);

    ulp_wakeup_reason = 0;

    esp_deep_sleep_disable_rom_logging();  // suppress some boot messages
    ESP_ERROR_CHECK(ulp_run(&ulp_entry - RTC_SLOW_MEM));
}

void enter_sleep() {
    gpio_uninstall_isr_service();

    SIM7000_sleep();
    ulp_start();

    ESP_LOGI(TAG, "Enter deep sleep");
    ESP_ERROR_CHECK(esp_sleep_enable_ulp_wakeup());
    esp_deep_sleep_start();
}

void app_after_sleep_init() {
    if (!full_init) {
        ESP_LOGI(TAG, "after sleep init");
        gpio_install_isr_service(0);
    }

    xTaskCreatePinnedToCore(SIM7000_after_sleep_init, "SIM7000_after_sleep_init", 1024 * 3, NULL, 0, NULL, M365_CPU);
    while (sim7000_mutex == NULL) {
        delay(10);
    }

    I2C_after_sleep_init();
    BMX160_after_sleep_init();
    BQ21061_after_sleep_init();
    NJU72501_after_sleep_init();
    RSA_after_sleep_init();
    M365_after_sleep_init();
    adsero_after_sleep_init();

    if (!full_init) {
        ESP_LOGI(TAG, "after sleep init finished");
    }
}
void app_full_init() {
    full_init = true;
    ESP_LOGI(TAG, "full init");
    gpio_install_isr_service(0);

    xTaskCreatePinnedToCore(SIM7000_full_init, "SIM7000_full_init", 1024 * 3, NULL, 0, NULL, M365_CPU);
    while (sim7000_mutex == NULL) {
        delay(10);
    }

    I2C_full_init();
    BMX160_full_init();
    BQ21061_full_init();
    NJU72501_full_init();
    RSA_full_init();
    M365_full_init();
    adsero_full_init();
    ulp_init();

    ESP_LOGI(TAG, "full init finished");
}

void app_main() {
    CLEAR_PERI_REG_MASK(RTC_CNTL_STATE0_REG, RTC_CNTL_ULP_CP_SLP_TIMER_EN);
    /* Esp-idf entry point */

    /* Debug, show frequency */
    rtc_cpu_freq_config_t old_config;
    rtc_clk_cpu_freq_get_config(&old_config);

    ESP_LOGI(TAG, "Main app, running freq %d", old_config.freq_mhz);
    ESP_LOGI(TAG, "Core id: %d", xPortGetCoreID());
    /* End debug */

    adc_power_off();

    esp_sleep_wakeup_cause_t wakeup_reason;
    wakeup_reason = esp_sleep_get_wakeup_cause();
    uint16_t ulp_reason = NOT_BY_ULP;

    if (wakeup_reason == ESP_SLEEP_WAKEUP_ULP) {
        needs_full_init = false;
        ulp_reason = (volatile uint16_t)ulp_wakeup_reason;

        if (ulp_reason == POWER_GOOD) {
            ESP_LOGW(TAG, "Wakeup caused by ULP, reason: POWER_GOOD");
        } else if (ulp_reason == ACCEL_INT) {
            ESP_LOGW(TAG, "Wakeup caused by ULP, reason: ACCEL_INT");
        } else if (ulp_reason == SIM7000) {
            ESP_LOGW(TAG, "Wakeup caused by ULP, reason: SIM7000");
        } else {
            ESP_LOGW(TAG, "Wakeup caused by ULP, reason %d", ulp_reason);
        }
    } else if (wakeup_reason == ESP_SLEEP_WAKEUP_TIMER) {
        needs_full_init = false;
        ESP_LOGW(TAG, "Wakeup caused by TIMER");
    } else {
        ESP_LOGW(TAG, "Was not in deep sleep before wakeup");
        needs_full_init = true;
    }

    storage_full_init();

    if (ulp_reason == ACCEL_INT && storage.adsero_status == LOCKED) {
        storage.adsero_status = STOLEN;
        storage.adsero_status_overwrite_server = true;
        storage_dirty = true;
    }
    if (needs_full_init) {
        app_full_init();
    } else {
        app_after_sleep_init();
    }

    delay(100);
    while (1) {
        if (atomic_load(&init_counter) == 0) {
            break;
        } else {
            delay(500);
        }
    }

    if (storage.adsero_status == STOLEN && storage.adsero_status_overwrite_server) {
        buzzer_config b = {
            .channel = BUZZER_BOTH,
            .time_ms = 10,
            .frequency = DEFAULT_BUZZER_FREQUENCY,
        };
        for (int k = 0; k < 10; k++) {
            for (int i = DEFAULT_BUZZER_FREQUENCY; i < DEFAULT_BUZZER_FREQUENCY + 1000; i += 200) {
                uint32_t start = GET_TIME();
                for (int ii = 0; ii < 200; ii += 20) {
                    b.frequency = i + ii;
                    enqueue_beep_config(&b);
                }
                delay(10 * 10 - (GET_TIME() - start));
            }
        }
    }
    if (storage_dirty) {
        storage_save();
    }

    ESP_LOGW(TAG, "init finished");

    beep_ms(10);
    delay(100);

    if (storage.adsero_status != DISABLED &&
        storage.adsero_status != LOCKED &&
        storage.adsero_status != STOLEN &&
        (ulp_reason == SIM7000 || ulp_reason == NOT_BY_ULP)) {
        uint8_t data = 0;
        xQueueSend(sim7000_int_queue, &data, portMAX_DELAY);
    }

    delay(1000);

    while (1) {
        if (atomic_load(&can_sleep) == 0) {
            break;
        } else {
            delay(10000);
        }
    }

    enter_sleep();
}
